package com.logsys.mvc.dao;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.logsys.mvc.model.SEO;
import com.logsys.utils.enumerations.SEOType;

public interface SEORepository extends BaseRepository<SEO>{

	@Query("SELECT s FROM SEO s WHERE s.url = ?1")
	public SEO findOne(String url);
	
	@Query("SELECT s FROM SEO s WHERE s.id = ?1")
	public SEO findOne(Serializable id);
	
	@Query("select s from SEO s")
	public Page<SEO> findAll(Pageable pageRequest);
	
	public Page<SEO> findSEOByType(SEOType type, Pageable pageRequest);
	
	@Query("SELECT CASE WHEN COUNT(s) > 0 THEN 'true' ELSE 'false' END FROM SEO s WHERE  s.id = ?1 and s.url = ?2")
	public boolean isExisted(Serializable id, String url);
}
