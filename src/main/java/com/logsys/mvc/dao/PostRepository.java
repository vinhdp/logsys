package com.logsys.mvc.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.logsys.mvc.model.Post;
import com.logsys.mvc.model.User;
import com.logsys.utils.enumerations.PostStatus;

public interface PostRepository extends BaseRepository<Post>, CustomPostRepository{

	@Query("select p from Post p where p.status = ?1 order by p.createdDate desc")
	public Page<Post> findPostByStatus(PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p where p.category.id = ?1 and p.status = ?2 order by p.createdDate desc")
	public Page<Post> findPostByCategory(Serializable id, PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p where p.status = ?1 order by p.view desc, p.createdDate desc")
	public List<Post> findTopPostByView(PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p where p.status = ?1 order by p.like desc, p.createdDate desc")
	public List<Post> findTopPostByLike(PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p where p.status = ?1 order by p.share desc, p.createdDate desc")
	public List<Post> findTopPostByShare(PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p where p.status = ?1 order by p.createdDate desc")
	public List<Post> findNewPost(PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p where p.status = ?1 order by p.share desc, p.createdDate desc")
	public List<Post> findRandomPost(PostStatus status, Pageable pageRequest);
	
	@Query("select p from Post p order by p.createdDate desc")
	public Page<Post> findAllPost(Pageable pageRequest);
	
	@Query("select p from Post p where p.alias = ?1 and p.status = ?2 order by p.createdDate desc")
	public Post findPostByAlias(String alias, PostStatus status);
	
	@Query("select p from Post p where p.alias = ?1 order by p.createdDate desc")
	public Post findPostByAlias(String alias);
	
	/***
	 * Moderator methods
	 */
	@Query("select p from Post p where p.author.email = ?1 order by p.createdDate desc")
	public Page<Post> findAllPost(String userEmail, Pageable pageRequest);
}
