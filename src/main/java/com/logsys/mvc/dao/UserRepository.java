package com.logsys.mvc.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.logsys.mvc.model.User;
import com.logsys.utils.enumerations.UserStatus;

public interface UserRepository extends BaseRepository<User>{

	public Page<User> findUserByStatus(UserStatus status, Pageable pageRequest);
	
	@Query("SELECT u FROM User u order by u.id desc")
	public Page<User> findAllUsers(Pageable pageRequest);
	
	public User findUserByEmail(String email);
}
