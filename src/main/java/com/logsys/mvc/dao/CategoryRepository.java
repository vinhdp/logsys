package com.logsys.mvc.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.utils.enumerations.CategoryStatus;

public interface CategoryRepository extends BaseRepository<Category>{

	/**
	 * Find all category that allowed to show in main menu
	 * @return
	 */
	@Query("SELECT c FROM Category c WHERE c.menuOrder != 0 AND c.status = 1 order by c.menuOrder asc")
	public Iterable<Category> findMenuCategory();
	
	/**
	 * Find all sub-category that allowed to show in main menu depend on parent id
	 * @param id
	 * @return
	 */
	@Query("SELECT c FROM Category c WHERE c.parentOrder != 0 and c.superCategory.id = ?1 AND c.status = 1"
			+ "order by c.parentOrder asc")
	public Iterable<Category> findShowingSubCategory(Serializable id);
	
	@Query("SELECT c FROM Category c WHERE c.superCategory.id = ?1 AND c.status = ?2 order by c.id desc")
	public Page<Category> findSubCategory(Serializable id, CategoryStatus status, Pageable pageRequest);
	
	@Query("SELECT c FROM Category c WHERE c.superCategory.id = ?1 order by c.id desc")
	public Page<Category> findSubCategory(Serializable id, Pageable pageRequest);
	
	@Query("SELECT c FROM Category c WHERE c.superCategory.id = ?1 AND c.status = 1 order by c.id desc")
	public Page<Category> findSubCategoryRest(Serializable id, Pageable pageRequest);
	
	@Query("SELECT c FROM Category c WHERE c.status = ?1 order by c.id desc")
	public Page<Category> findCategory(CategoryStatus status, Pageable pageRequest);
	
	@Query("SELECT p FROM Post p WHERE p.category.id = ?1 order by p.view desc, p.createdDate desc")
	public List<Post> findRandomPost(Serializable id, Pageable pageRequest);
	
	@Query("SELECT c FROM Category c WHERE c.alias = ?1 order by c.id desc")
	public Category findOneByAlias(String alias);
	
	@Query("SELECT c FROM Category c WHERE c.alias = ?1 and c.status = ?2 order by c.id desc")
	public Category findOneByAlias(String alias, CategoryStatus status);
}
