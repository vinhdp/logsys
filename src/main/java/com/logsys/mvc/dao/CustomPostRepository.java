package com.logsys.mvc.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logsys.mvc.dto.PostDTO;
import com.logsys.utils.enumerations.PostStatus;

public interface CustomPostRepository {

	public Page<PostDTO> findPostByTitle(String userEmail, String title, Integer categoryId, PostStatus status, Pageable pageRequest);
	public int countPostByCategory(Integer categoryId, PostStatus status);
}
