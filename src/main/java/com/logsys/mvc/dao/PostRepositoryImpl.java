package com.logsys.mvc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.logsys.mvc.dto.PostDTO;
import com.logsys.mvc.mapper.PostMapper;
import com.logsys.mvc.model.Post;
import com.logsys.utils.enumerations.PostStatus;

@Repository
public class PostRepositoryImpl implements CustomPostRepository {

	@Autowired
	private EntityManager em;

	@Override
	public Page<PostDTO> findPostByTitle(String userEmail, String title, Integer categoryId, PostStatus status, Pageable pageRequest) {

		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Post> criteria = builder.createQuery(Post.class);
		Root<Post> root = criteria.from(Post.class);

		Predicate titleLike = builder.like(builder.lower(root.get("title")), "%" + title + "%");
		Predicate cateEq = builder.conjunction();
		Predicate emailEq = builder.conjunction();
		
		if(categoryId != 0){
			
			cateEq = builder.equal(root.get("category").get("id"), categoryId);
		}
		
		if(!"".equals(userEmail)){
			
			emailEq = builder.equal(root.get("author").get("email"), userEmail);
		}
		
		criteria.where(builder.and(titleLike, cateEq, emailEq));

		TypedQuery<Post> typedQuery = em.createQuery(criteria);
		List<Post> lstPosts = typedQuery.setFirstResult(pageRequest.getOffset())
				.setMaxResults(pageRequest.getPageSize()).getResultList();
		List<PostDTO> lstPostDTOs = PostMapper.INSTANCE.postsToPostDTOs(lstPosts);
		
		/* count total post */
		CriteriaQuery<Long> criteriaCount = builder.createQuery(Long.class);
		criteriaCount.multiselect(builder.count(criteriaCount.from(Post.class)));
		criteriaCount.where(builder.like(builder.lower(root.get("title")), "%" + title + "%"));
		
		if(categoryId != 0){
			
			criteriaCount.where(builder.equal(root.get("category").get("id"), categoryId));
		}
		
		if(!"".equals(userEmail)){
			criteriaCount.where(builder.equal(root.get("author").get("email"), userEmail));
		}
		
		criteriaCount.where(builder.and(titleLike, cateEq, emailEq));

		TypedQuery<Long> typedCountQuery = em.createQuery(criteriaCount);
		int total = typedCountQuery.getSingleResult().intValue();

		return new PageImpl<>(lstPostDTOs, pageRequest, total);
	}

	@Override
	public int countPostByCategory(Integer categoryId, PostStatus status) {
		
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaCount = builder.createQuery(Long.class);
		Root<Post> root = criteriaCount.from(Post.class);
		
		criteriaCount.multiselect(builder.count(criteriaCount.from(Post.class)));
		criteriaCount.where(builder.equal(root.get("status"), status));
		
		if(categoryId != 0){
			
			criteriaCount.where(builder.equal(root.get("category").get("id"), categoryId));
		}

		TypedQuery<Long> typedCountQuery = em.createQuery(criteriaCount);
		int total = typedCountQuery.getSingleResult().intValue();
		return total;
	}
}
