package com.logsys.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.logsys.mvc.model.User;

@Controller
public class LoginController {

	@RequestMapping("/login")
	public String getLogin(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			Model model){
		
		model.addAttribute("user", new User());
		
		if (error != null) {
			
			model.addAttribute("error", "Invalid username and password!");
		}else if(logout != null){
			
			model.addAttribute("msg", "You've been logged out successfully.");
		}else{
			
			model.addAttribute("message", "Sign in to start your session!");
		}
		return "login";
	}
}
