package com.logsys.mvc.controller;

import java.security.Principal;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class AuthenticationController {

	@ModelAttribute
	public void authenticationHandler(Principal principal, Model model){
		
		if(principal != null){
		
			model.addAttribute("authEmail", principal.getName());
		}
	}
}
