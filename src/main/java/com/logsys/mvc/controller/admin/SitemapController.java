package com.logsys.mvc.controller.admin;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.CategoryService;
import com.logsys.mvc.service.PostService;
import com.logsys.utils.enumerations.CategoryStatus;
import com.logsys.utils.enumerations.PostStatus;

@Controller
@RequestMapping("/inotes")
public class SitemapController {
	
	private static final int MAX_SITEMAP_ITEM = 1000;
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private PostService postService;

	@RequestMapping("/create-sitemap-index")
	@ResponseBody
	public Map<String, String> createSitemap(@RequestParam(name = "location", required = false) String location) throws ParserConfigurationException, TransformerException {
		
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);	// require for namespace
		DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		/** EDIT FROM HERE*/
		Map<String, String> mapIndex = new HashMap<String, String>();
		mapIndex.put("itechnotes.xml", "itechnotes.xml");
		
		/** CATEGORY SITEMAP */
		Iterable<Category> lstCategory = categoryService.findAll();
		for(Category category : lstCategory){
			
			int totalPost = postService.countPostByCategory(category.getId(), PostStatus.PUBLISHED);
			int totalFiles = (int) Math.ceil(totalPost * 1.0 / MAX_SITEMAP_ITEM);
			for(int i = 1; i<= totalFiles; i++){
				
				String fileName = category.getAlias() + "-" + i + ".xml";
				mapIndex.put(fileName, fileName);
			}
		}
		/** END CATEGORY SITEMAP */
		
		/** CREATE SITEMAP INDEX*/
		Document sitemapIndex = docBuilder.newDocument();
		Element rootElement = sitemapIndex.createElement("sitemapindex");
		rootElement.setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
		sitemapIndex.appendChild(rootElement);
		
		for(Entry<String, String> entry : mapIndex.entrySet()){
			
			Element sitemapElement = sitemapIndex.createElement("sitemap");
			Element locElement = sitemapIndex.createElement("loc");
			Element lastModElement = sitemapIndex.createElement("lastmod");
			locElement.setTextContent("http://itechnotes.com" + "/" + entry.getValue());
			lastModElement.setTextContent((new Date()).toString());
			sitemapElement.appendChild(locElement);
			sitemapElement.appendChild(lastModElement);
			
			// add sitemap
			rootElement.appendChild(sitemapElement);
		}
		
		DOMSource domSource = new DOMSource(sitemapIndex);
		String path = "";
		if(location != null && !"".equals(location)){
			path = location;
		}
		
		StreamResult result = new StreamResult(new File(path + File.separator + "sitemap_index.xml"));
		transformer.transform(domSource, result);
		return mapIndex;
	}
	
	@RequestMapping("/create-sitemap")
	@ResponseBody
	public String createDocumentSitemap(@RequestParam(name = "location", required = false) String location)
			throws ParserConfigurationException, TransformerException{
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
		
				try {
					
					DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
					documentBuilderFactory.setNamespaceAware(true);	// require for namespace
					DocumentBuilder docBuilder;
					
					docBuilder = documentBuilderFactory.newDocumentBuilder();
					
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					List<String> lstCategoryLink = new ArrayList<String>();
					lstCategoryLink.add("");
					
					Iterable<Category> lstCategory = categoryService.findAll();
					for(Category category : lstCategory){
						
						lstCategoryLink.add("/category/" + category.getAlias());
						
						int totalPost = postService.countPostByCategory(category.getId(), PostStatus.PUBLISHED);
						System.out.println(category.getAlias() + ": " + totalPost);
						int totalFile = (int) Math.ceil(totalPost * 1.0 / MAX_SITEMAP_ITEM);
						
						String postPath = "post/";
						if(category.getAlias().equals("information")){
							postPath = "";
						}
						
						for(int i = 1; i<= totalFile; i++){
							
							Document categorySitemap = docBuilder.newDocument();
							Element urlset = categorySitemap.createElement("urlset");
							urlset.setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
							urlset.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
							urlset.setAttribute("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
							categorySitemap.appendChild(urlset);
							
							Iterator<Post> lstPost = postService.findPostByCategory(category.getId(), PostStatus.PUBLISHED, new PageRequest(i - 1, MAX_SITEMAP_ITEM)).iterator();
							
							while(lstPost.hasNext()){
								
								Post post = lstPost.next();
								Element url = categorySitemap.createElement("url");
								Element loc = categorySitemap.createElement("loc");
								loc.setTextContent("http://itechnotes.com/" + postPath + post.getAlias());
								url.appendChild(loc);
								
								Element changefreq = categorySitemap.createElement("changefreq");
								changefreq.setTextContent("daily");
								url.appendChild(changefreq);
								
								Element priority = categorySitemap.createElement("priority");
								priority.setTextContent("1");
								url.appendChild(priority);
								
								urlset.appendChild(url);
							}
							
							String path = "";
							if(location != null && !"".equals(location)){
								path = location;
							}
							
							String fileName = category.getAlias() + "-" + i + ".xml";
							System.out.println("-----> " + fileName);
							DOMSource domSource = new DOMSource(categorySitemap);
							StreamResult result = new StreamResult(new File(path + File.separator + fileName));
							transformer.transform(domSource, result);
						}
					}
					
					// category site map
					Document categorySitemap = docBuilder.newDocument();
					Element urlset = categorySitemap.createElement("urlset");
					urlset.setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
					urlset.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
					urlset.setAttribute("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
					categorySitemap.appendChild(urlset);
					
					for(String link : lstCategoryLink) {
						Element url = categorySitemap.createElement("url");
						Element loc = categorySitemap.createElement("loc");
						loc.setTextContent("http://itechnotes.com" + link);
						url.appendChild(loc);
						
						Element changefreq = categorySitemap.createElement("changefreq");
						changefreq.setTextContent("daily");
						url.appendChild(changefreq);
						
						Element priority = categorySitemap.createElement("priority");
						priority.setTextContent("0.8");
						url.appendChild(priority);
						
						urlset.appendChild(url);
					}
					
					String fileName = "itechnotes.xml";
					System.out.println("-----> " + fileName);
					DOMSource domSource = new DOMSource(categorySitemap);
					StreamResult result = new StreamResult(new File(location + File.separator + fileName));
					transformer.transform(domSource, result);
					
				} catch (ParserConfigurationException | TransformerException e) {
					e.printStackTrace();
				}
			}
		});
		
		thread.start();
		return "Thread Submitted";
	}
}
