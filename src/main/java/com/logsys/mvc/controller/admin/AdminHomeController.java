package com.logsys.mvc.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/inotes")
public class AdminHomeController {

	@RequestMapping("/")
	public String getAdminHome(Model model){
		
		return "admin_home";
	}
}
