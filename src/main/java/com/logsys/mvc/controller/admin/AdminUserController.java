package com.logsys.mvc.controller.admin;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.logsys.mvc.dto.UserDTO;
import com.logsys.mvc.dto.UserPasswordDTO;
import com.logsys.mvc.mapper.UserMapper;
import com.logsys.mvc.model.User;
import com.logsys.mvc.service.UserService;

@Controller
@RequestMapping("/inotes")
public class AdminUserController {

	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	public String getAllUsers(@PageableDefault(value = 20) Pageable pageRequest, Model model) {

		Page<User> lstUsers = userService.findAllUsers(pageRequest);
		model.addAttribute("lstUsers", lstUsers.iterator());
		model.addAttribute("totalPages", lstUsers.getTotalPages());
		model.addAttribute("pageNumber", lstUsers.getNumber());
		model.addAttribute("totalElements", lstUsers.getTotalElements());
		model.addAttribute("pageSize", lstUsers.getSize());
		return "admin_user";
	}

	@RequestMapping("/user/{id}")
	public String getUser(@PathVariable Integer id, Model model) {

		User user = userService.findOne(id);
		UserDTO userDTO = UserMapper.INSTANCE.userToUserDTO(user);

		model.addAttribute("userDTO", userDTO);
		return "admin_user_detail";
	}

	@RequestMapping(value = "/user/add", method = RequestMethod.GET)
	public String getAddUser(Model model) {

		UserDTO userDTO = new UserDTO();

		model.addAttribute("userDTO", userDTO);
		return "admin_user_add";
	}

	@RequestMapping(value = "/user/add", method = RequestMethod.POST)
	public String addUser(@Valid @ModelAttribute UserDTO userDTO, BindingResult result, Model model,
			RedirectAttributes attributes) {

		model.addAttribute("userDTO", userDTO);

		if (result.hasErrors()) {

			return "admin_user_add";
		}

		User user = UserMapper.INSTANCE.userDTOToUser(userDTO);
		user.setCreatedDate(new Date());
		user = userService.save(user);

		attributes.addFlashAttribute("success", 1);
		return "redirect:" + user.getId();
	}

	@RequestMapping(value = "/user/{id}/update", method = RequestMethod.GET)
	public String getUpdateUser(@PathVariable Integer id, Model model) {

		User user = userService.findOne(id);
		UserDTO userDTO = UserMapper.INSTANCE.userToUserDTO(user);

		model.addAttribute("userDTO", userDTO);
		return "admin_user_update";
	}

	@RequestMapping(value = "/user/{id}/update", method = RequestMethod.POST)
	public String updateUser(@PathVariable Integer id, @Valid @ModelAttribute UserDTO userDTO, BindingResult result,
			Model model) {

		model.addAttribute("userDTO", userDTO);

		if (result.hasErrors()) {

			return "admin_user_update";
		}

		User user = UserMapper.INSTANCE.userDTOToUser(userDTO);
		User dbUser = userService.findOne(user.getId());
		
		user.setPassword(dbUser.getPassword());
		userService.save(user);

		return "redirect:../" + userDTO.getId();
	}

	@RequestMapping("/user/{id}/delete")
	public String deleteUser(@PathVariable Integer id, Model model) {

		User user = userService.findOne(id);
		userService.delete(user);
		return "redirect:../../users";
	}

	@RequestMapping("/user/{email}/reset-password")
	public String getChangePassword(@PathVariable String email, Model model) {

		User user = userService.findUserByEmail(email);
		UserPasswordDTO userDTO = new UserPasswordDTO(user);

		model.addAttribute("userPasswordDTO", userDTO);
		model.addAttribute("firstName", user.getFirstName());
		model.addAttribute("lastName", user.getLastName());
		return "admin_user_password";
	}

	@RequestMapping(value = "/user/{uemail}/reset-password", method = RequestMethod.POST)
	public String changePassword(@Valid @ModelAttribute UserPasswordDTO userPasswordDTO, @PathVariable String uemail,
			BindingResult result, Model model) {

		if (result.hasErrors()) {

			return "admin_user_password";
		}
		
		String email = userPasswordDTO.getEmail();
		String password = userPasswordDTO.getPassword();
		String newPass = userPasswordDTO.getNewPassword();
		String confirmPass = userPasswordDTO.getConfirmPassword();

		User dbUser = userService.findUserByEmail(email);
		model.addAttribute("userPasswordDTO", userPasswordDTO);
		model.addAttribute("firstName", userPasswordDTO.getFirstName());
		model.addAttribute("lastName", userPasswordDTO.getLastName());
		
		if(newPass.equals(confirmPass)){
			
			if(dbUser != null && encoder.matches(password, dbUser.getPassword())){
				
				newPass = encoder.encode(newPass);
				dbUser.setPassword(newPass);
				userService.save(dbUser);
			} else {
				
				result.rejectValue("email", "error");
				return "admin_user_password";
			}
		} else {
			
			result.rejectValue("confirmPassword", "mismatch");
			return "admin_user_password";
		}

		model.addAttribute("success", 1);
		return "admin_user_password";
	}
	
	@RequestMapping("/user/{email}/edit-permission")
	public String getEditPermission(@PathVariable String email, Model model) {

		User user = userService.findUserByEmail(email);
		model.addAttribute("email", user.getEmail());
		model.addAttribute("firstName", user.getFirstName());
		model.addAttribute("lastName", user.getLastName());
		return "admin_user_permission";
	}
}
