package com.logsys.mvc.controller.admin;

import java.security.Principal;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logsys.mvc.dto.PostDTO;
import com.logsys.mvc.mapper.PostMapper;
import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.mvc.model.User;
import com.logsys.mvc.service.CategoryService;
import com.logsys.mvc.service.PostService;
import com.logsys.mvc.service.UserService;
import com.logsys.utils.enumerations.PostStatus;

@Controller
@RequestMapping("/inotes")
public class AdminPostController {

	@Autowired
	PostService postService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping("/posts")
	public String getAllPost(@PageableDefault(value = 20) Pageable pageRequest, Model model){
		
		Page<Post> lstPosts = postService.findAllPost(pageRequest);
		Iterable<Category> lstCategories = categoryService.findAll();
		
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("lstPosts", lstPosts.iterator());
		model.addAttribute("totalPages", lstPosts.getTotalPages());
		model.addAttribute("pageNumber", lstPosts.getNumber());
		model.addAttribute("totalElements", lstPosts.getTotalElements());
		model.addAttribute("pageSize", lstPosts.getSize());
		return "admin_post";
	}
	
	@RequestMapping("/post/{id}")
	public String getPost(@PathVariable Integer id, Model model){

		Post post = postService.findOne(id);
		PostDTO postDTO = PostMapper.INSTANCE.postToPostDTO(post);
		Iterable<Category> lstCategories = categoryService.findAll();
		postDTO.setCategoryId(post.getCategory().getId());
		
		model.addAttribute("seoId", post.getSeo() == null ? 0 : post.getSeo().getId());
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("postDTO", postDTO);
		return "admin_post_detail";
	}
	
	@RequestMapping("/post/{id}/add")
	public String getAddPost(@PathVariable Integer id, Model model){
		
		PostDTO postDTO = new PostDTO();
		postDTO.setCategoryId(id);
		postDTO.setStatus(PostStatus.NEW);
		Iterable<Category> lstCategories = categoryService.findAll();
		
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("postDTO", postDTO);
		return "admin_post_add";
	}
	
	@RequestMapping(value = "/post/{id}/add", method = RequestMethod.POST)
	public String addPost(@PathVariable Integer id, @Valid @ModelAttribute PostDTO postDTO,
			BindingResult result, Model model, Principal principal){
		
		Iterable<Category> lstCategories = categoryService.findAll();
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("postDTO", postDTO);
		
		if(result.hasErrors()){
			
			return "admin_post_add";
		}
		
		Post dbPost = postService.findPostByAlias(postDTO.getAlias());
		
		if(dbPost != null){
			
			result.rejectValue("alias", "exist");
			return "admin_post_add";
		}
		
		String image = "";
		
		Post post = PostMapper.INSTANCE.postDTOToPost(postDTO);
		Category category = categoryService.findOne(postDTO.getCategoryId());
		image = category.getImage();
		
		post.setCategory(category);
		User user = userService.findUserByEmail(principal.getName());
		post.setAuthor(user);
		Date createdDate = new Date();
		post.setCreatedDate(createdDate);
		
		if("".equals(post.getImage()) && !"".equals(image)){
			post.setImage(image);
		}
		
		if(postDTO.getPublishedDate() == null && postDTO.getStatus() == PostStatus.PUBLISHED){
			
			post.setPublishedDate(createdDate);
		}
		
		post = postService.save(post);
		
		return "redirect:../" + post.getId();
	}
	
	@RequestMapping("/post/{id}/update")
	public String getUpdatePost(@PathVariable Integer id, Model model){
		
		Post post = postService.findOne(id);
		PostDTO postDTO = PostMapper.INSTANCE.postToPostDTO(post);
		Iterable<Category> lstCategories = categoryService.findAll();
		postDTO.setCategoryId(post.getCategory().getId());
		
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("postDTO", postDTO);
		return "admin_post_update";
	}
	
	@RequestMapping(value = "/post/{id}/update", method = RequestMethod.POST)
	public String updatePost(@PathVariable Integer id, @Valid @ModelAttribute PostDTO postDTO, BindingResult result, Model model){
		
		Iterable<Category> lstCategories = categoryService.findAll();
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("postDTO", postDTO);
		
		if(result.hasErrors()){
			
			return "admin_post_update";
		}
		
		Post dbPost = postService.findPostByAlias(postDTO.getAlias());
		
		if(dbPost != null && dbPost.getId() != postDTO.getId()){
			
			result.rejectValue("alias", "exist");
			return "admin_post_update";
		}
		
		Post post = PostMapper.INSTANCE.postDTOToPost(postDTO);
		Post oldPost = postService.findOne(post.getId());
		Category category = categoryService.findOne(postDTO.getCategoryId());
		post.setCategory(category);
		post.setAuthor(oldPost.getAuthor());
		
		Date lastUpdateDate = new Date();
		post.setModifiedDate(lastUpdateDate);
		
		if(postDTO.getPublishedDate() == null && postDTO.getStatus() == PostStatus.PUBLISHED){
			
			post.setPublishedDate(lastUpdateDate);
		}
		
		post = postService.save(post);
		
		return "redirect:../" + post.getId();
	}
	
	@RequestMapping("/post/{id}/delete")
	public String deletePost(@PathVariable Integer id, Model model){
		
		try{
			
			postService.delete(postService.findOne(id));
		}catch(DataIntegrityViolationException ex){
			
			ex.printStackTrace();
		}
		
		return "redirect:../../posts";
	}
	
	@ResponseBody
	@RequestMapping("/post/rest/category/{id}")
	public Page<PostDTO> restAdminPostSearching(@RequestParam String title, @PathVariable("id") Integer id,
			@PageableDefault(value = 20) Pageable pageRequest, Model model,
			Principal pricipal){
		
		Page<PostDTO> lstPosts = postService.findPostByTitle("", title, id, PostStatus.PUBLISHED, pageRequest);
		return lstPosts;
	}
}
