package com.logsys.mvc.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.logsys.mvc.dto.CategoryDTO;
import com.logsys.mvc.mapper.CategoryMapper;
import com.logsys.mvc.model.Category;
import com.logsys.mvc.service.CategoryService;

@Controller
@RequestMapping("/inotes")
public class AdminCategoryController {

	@Autowired
	CategoryService categoryService;
	
	@RequestMapping("/categories")
	public String getAllCategory(Model model){
		
		Iterable<Category> lstCategories = categoryService.findAll();
		
		model.addAttribute("lstCategories", lstCategories);
		return "admin_category";
	}
	
	@RequestMapping("/category/{id}")
	public String getCategory(@PathVariable Integer id, Model model){
		
		Category category = categoryService.findOne(id);
		CategoryDTO categoryDTO = CategoryMapper.INSTANCE.categoryToCategoryDTO(category);
		Category superCategory = category.getSuperCategory();
		Integer superId = superCategory == null ? 0 : superCategory.getId();
		categoryDTO.setSuperCategoryId(superId);
		
		Iterable<Category> lstCategories = categoryService.findAll();
		Iterable<Category> lstSubCategories = category.getSubCategories();
		
		model.addAttribute("seoId", category.getSeo() == null ? 0 : category.getSeo().getId());
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("categoryDTO", categoryDTO);
		model.addAttribute("lstSubCategories", lstSubCategories);
		return "admin_category_detail";
	}
	
	@RequestMapping("/category/{id}/add")
	public String getAddCategory(@PathVariable Integer id, Model model){
		
		/* required */
		CategoryDTO categoryDTO = new CategoryDTO();
		categoryDTO.setStatus("SHOW");
		categoryDTO.setSuperCategoryId(id);
		
		Category parentCategory = categoryService.findOne(id);
		
		if(parentCategory == null){
			
			parentCategory = new Category();
		}
		Iterable<Category> lstCategories = categoryService.findAll();
		
		model.addAttribute("categoryDTO", categoryDTO);
		model.addAttribute("parentCategory", parentCategory);
		model.addAttribute("lstCategories", lstCategories);
		return "admin_category_add";
	}
	
	@RequestMapping(value = "/category/{id}/add", method = RequestMethod.POST)
	public String addCategory(@PathVariable Integer id, @Valid @ModelAttribute("categoryDTO") CategoryDTO categoryDTO, BindingResult result, Model model){

		/* required */
		Category parentCategory = categoryService.findOne(id);
		
		if(parentCategory == null){
			
			parentCategory = new Category();
		}
		
		Iterable<Category> lstCategories = categoryService.findAll();
		model.addAttribute("parentCategory", parentCategory);
		model.addAttribute("lstCategories", lstCategories);
		
		if(result.hasErrors()){
			
			return "admin_category_add";
		}
		
		Category dbCategory = categoryService.findOneByAlias(categoryDTO.getAlias());
		
		if(dbCategory != null){
			
			result.rejectValue("alias", "exist");
			return "admin_category_add";
		}
		
		Category category = CategoryMapper.INSTANCE.categoryDTOToCategory(categoryDTO);
		Category superCategory = categoryService.findOne(categoryDTO.getSuperCategoryId());
		category.setSuperCategory(superCategory);
		category.setId(0);	// make category as new category after mapping
		
		category = categoryService.save(category);
		
		model.addAttribute("category", categoryDTO);
		return "redirect:../" + category.getId();
	}
	
	@RequestMapping("/category/{id}/update")
	public String getUpdateCategory(@PathVariable Integer id, Model model){
		
		Category category = categoryService.findOne(id);
		CategoryDTO categoryDTO = CategoryMapper.INSTANCE.categoryToCategoryDTO(category);
		Category superCategory = category.getSuperCategory();
		Integer superId = superCategory == null ? 0 : superCategory.getId();
		categoryDTO.setSuperCategoryId(superId);
		
		Iterable<Category> lstCategories = categoryService.findAll();
		Iterable<Category> lstSubCategories = category.getSubCategories();
		
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("categoryDTO", categoryDTO);
		model.addAttribute("lstSubCategories", lstSubCategories);
		
		return "admin_category_update";
	}
	

	@RequestMapping(value = "/category/{id}/update", method = RequestMethod.POST)
	public String updateCategory(@PathVariable Integer id, @Valid @ModelAttribute CategoryDTO categoryDTO,
			BindingResult result, Model model){
		
		/* required */
		Iterable<Category> lstCategories = categoryService.findAll();
		Iterable<Category> lstSubCategories = categoryService.findSubCategory(id, new PageRequest(0, 100));
		
		model.addAttribute("lstCategories", lstCategories);
		model.addAttribute("categoryDTO", categoryDTO);
		model.addAttribute("lstSubCategories", lstSubCategories.iterator());
		
		if(result.hasErrors()){
			
			return "admin_category_update";
		}
		
		Category dbCategory = categoryService.findOne(categoryDTO.getId());
		
		if(dbCategory != null && dbCategory.getId() != categoryDTO.getId()){
			
			result.rejectValue("alias", "exist");
			return "admin_category_update";
		}
		System.out.println(categoryDTO.getName());
		Category category = CategoryMapper.INSTANCE.categoryDTOToCategory(categoryDTO);
		Category superCategory = categoryService.findOne(categoryDTO.getSuperCategoryId());
		category.setSuperCategory(superCategory);
		category = categoryService.save(category);
		
		return "redirect:../" + category.getId();
	}
	
	@RequestMapping("/category/{id}/delete")
	public String deleteCategory(@PathVariable Integer id, Model model){
		
		try{
			
			categoryService.delete(categoryService.findOne(id));
		}catch(DataIntegrityViolationException ex){
			
			ex.printStackTrace();
		}
		
		return "redirect:../../categories";
	}
}
