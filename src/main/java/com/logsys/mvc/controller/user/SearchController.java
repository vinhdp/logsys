package com.logsys.mvc.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController {

	@RequestMapping("/search")
	public String search(@RequestParam String q, Model model){
		
		model.addAttribute("q", q);
		return "search";
	}
}
