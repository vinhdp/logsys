package com.logsys.mvc.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.CategoryService;
import com.logsys.mvc.service.PostService;
import com.logsys.utils.enumerations.CategoryStatus;
import com.logsys.utils.enumerations.PostStatus;

@Controller
public class HomeController {

	@Autowired
	private PostService postService;
	
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping("/")
	public String home(Model model, @PageableDefault(value = 9) Pageable pageRequest) {

		Page<Category> lstCategories = categoryService.findCategory(CategoryStatus.SHOW, pageRequest);
		
		Pageable pageReq = new PageRequest(0, 5);
		List<Post> lstTopPosts = postService.findTopPostByView(PostStatus.PUBLISHED, pageReq);
		List<Post> lstNewPosts = postService.findNewPost(PostStatus.PUBLISHED, new PageRequest(0, 8));
		
		model.addAttribute("lstNewPosts", lstNewPosts);
		model.addAttribute("lstTopPosts", lstTopPosts);
		model.addAttribute("lstCategories", lstCategories.iterator());
		model.addAttribute("totalPages", lstCategories.getTotalPages());
		model.addAttribute("pageNumber", lstCategories.getNumber());
		return "home";
	}
}
