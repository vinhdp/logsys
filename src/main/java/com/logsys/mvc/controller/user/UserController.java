package com.logsys.mvc.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logsys.mvc.model.User;
import com.logsys.mvc.service.UserService;
import com.logsys.utils.enumerations.UserStatus;

public class UserController {
	
	@Autowired
	private UserService userService;

	@RequestMapping("/members")
	public String user(@PageableDefault(value = 10) Pageable pageRequest, Model model) {

		Page<User> lstMembers = userService.findUserByStatus(UserStatus.ACTIVE, pageRequest);

		model.addAttribute("lstMembers", lstMembers.iterator());
		model.addAttribute("totalPages", lstMembers.getTotalPages());
		model.addAttribute("pageNumber", pageRequest.getPageNumber());
		return "member";
	}
}
