package com.logsys.mvc.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.CategoryService;
import com.logsys.mvc.service.PostService;
import com.logsys.mvc.service.SEOService;
import com.logsys.utils.enumerations.PostStatus;

@Controller
public class PostController {
	
	@Autowired
	private CategoryService categoryService;

	@Autowired
	private PostService postService;
	
	@Autowired
	private SEOService seoService;
	
	@RequestMapping("/posts")
	public String post(@PageableDefault(value = 10) Pageable pageRequest, Model model) {

		Page<Post> lstPosts = postService.findPostByStatus(PostStatus.PUBLISHED, pageRequest);

		model.addAttribute("lstPosts", lstPosts.iterator());
		model.addAttribute("totalPages", lstPosts.getTotalPages());
		model.addAttribute("pageNumber", lstPosts.getNumber());
		return "post";
	}
	
	@RequestMapping("/{category}/{alias}")
	public String postDetail(@PathVariable String alias, Model model){
		
		Post post = postService.findPostByAlias(alias, PostStatus.PUBLISHED);
		
		Category category = post.getCategory();
		Iterable<Category> lstAncestors = categoryService.findCategoryAncestors(category.getId());

		Pageable pageRequest = new PageRequest(0, 5);
		List<Post> lstRelatedPosts = categoryService.findRandomPost(category.getId(), pageRequest);
		List<Post> lstTopPosts = postService.findTopPostByView(PostStatus.PUBLISHED, pageRequest);
		
		model.addAttribute("lstTopPosts", lstTopPosts);
		model.addAttribute("lstRelatedPosts", lstRelatedPosts);
		model.addAttribute("lstAncestors", lstAncestors);
		model.addAttribute("post", post);
		model.addAttribute("seoInfo", post.getSeo() == null ? seoService.findOne("/seo-default") : post.getSeo());
		return "post_detail";
	}
}
