package com.logsys.mvc.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.PostService;
import com.logsys.mvc.service.SEOService;
import com.logsys.utils.enumerations.PostStatus;

@Controller
public class WebsiteInfoController {

	@Autowired
	private PostService postService;
	
	@Autowired
	private SEOService seoService;
	
	@RequestMapping("/{alias}")
	public String getSiteInfo(@PathVariable String alias, Model model){
		
		Post post = postService.findPostByAlias(alias, PostStatus.PUBLISHED);
		if(post == null){
			return "redirect:/error/404";
		}
		model.addAttribute("post", post);
		model.addAttribute("seoInfo", post.getSeo() == null ? seoService.findOne("/seo-default") : post.getSeo());
		return "site_info";
	}
}
