package com.logsys.mvc.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.CategoryService;
import com.logsys.mvc.service.PostService;
import com.logsys.mvc.service.SEOService;
import com.logsys.utils.enumerations.CategoryStatus;
import com.logsys.utils.enumerations.PostStatus;

@Controller
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;

	@Autowired
	private PostService postService;
	
	@Autowired
	private SEOService seoService;
	
	@RequestMapping("/categories")
	public String getAllCategory(@PageableDefault(value = 9) Pageable pageRequest, Model model){
		
		Page<Category> lstCategories = categoryService.findCategory(CategoryStatus.SHOW, pageRequest);
		
		model.addAttribute("lstCategories", lstCategories.iterator());
		model.addAttribute("totalPages", lstCategories.getTotalPages());
		model.addAttribute("pageNumber", lstCategories.getNumber());
		
		return "category";
	}
	
	@RequestMapping("/category/{alias}")
	public String category(@PathVariable("alias") String alias, Pageable pageRequest, Model model) {

		Category category = categoryService.findOneByAlias(alias, CategoryStatus.SHOW);
		
		if(category == null){
			
			return "redirect:/error/404";
		}
		
		Page<Category> lstSubCategories = categoryService.findSubCategory(category.getId(), CategoryStatus.SHOW, pageRequest);
		Iterable<Category> lstAncestors = categoryService.findCategoryAncestors(category.getId());

		Pageable pageReq = new PageRequest(0, 5);
		List<Post> lstRelatedPosts = categoryService.findRandomPost(category.getId(), pageReq);
		List<Post> lstTopPosts = postService.findTopPostByView(PostStatus.PUBLISHED, pageReq);
		Post post = postService.findPostByAlias(alias);
		
		model.addAttribute("post", post);
		model.addAttribute("lstTopPosts", lstTopPosts);
		model.addAttribute("lstRelatedPosts", lstRelatedPosts);
		
		model.addAttribute("category", category);
		model.addAttribute("lstAncestors", lstAncestors.iterator());
		model.addAttribute("seoInfo", category.getSeo() == null ? seoService.findOne("/seo-default") : category.getSeo());

		if (lstSubCategories.getTotalElements() != 0) {

			model.addAttribute("lstSubCategories", lstSubCategories.iterator());
			model.addAttribute("totalPages", lstSubCategories.getTotalPages());
			model.addAttribute("pageNumber", lstSubCategories.getNumber());

			return "category_detail";
		}
		// get post
		Page<Post> lstPosts = postService.findPostByCategory(category.getId(), PostStatus.PUBLISHED, pageRequest);
		model.addAttribute("lstPosts", lstPosts.iterator());
		model.addAttribute("totalPages", lstPosts.getTotalPages());
		model.addAttribute("pageNumber", lstPosts.getNumber());
		model.addAttribute("totalElements", lstPosts.getTotalElements());
		model.addAttribute("pageSize", lstPosts.getSize());
		
		return "sub_category_detail";
	}
	
	@ResponseBody
	@RequestMapping("/rest/category/{id}")
	public Page<Category> restSubCategories(@PathVariable("id") Integer id, @PageableDefault(value = 9) Pageable pageRequest, Model model){
		
		Page<Category> lstSubCategories = categoryService.findSubCategoryRest(id, pageRequest);
		return lstSubCategories;
	}
	
	@ResponseBody
	@RequestMapping("/rest/category/{id}/post")
	public Page<Post> restCategoryPosts(@PathVariable("id") Integer id, @PageableDefault(value = 9) Pageable pageRequest, Model model){
		
		Page<Post> lstPosts = postService.findPostByCategory(id, PostStatus.PUBLISHED, pageRequest);
		return lstPosts;
	}
}
