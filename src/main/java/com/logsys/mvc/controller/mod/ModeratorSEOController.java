package com.logsys.mvc.controller.mod;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.mvc.model.SEO;
import com.logsys.mvc.service.CategoryService;
import com.logsys.mvc.service.PostService;
import com.logsys.mvc.service.SEOService;

@Controller
@RequestMapping("/itechs")
public class ModeratorSEOController {

	@Autowired
	private SEOService seoService;
	
	@Autowired
	private PostService postService;
	
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping("/seos")
	public String getSEO(@PageableDefault(value = 20) Pageable pageRequest, Model model){
		
		Page<SEO> lstSEOs = seoService.findAll(pageRequest);
		
		model.addAttribute("lstSEOs", lstSEOs.iterator());
		model.addAttribute("totalPages", lstSEOs.getTotalPages());
		model.addAttribute("pageNumber", lstSEOs.getNumber());
		model.addAttribute("totalElements", lstSEOs.getTotalElements());
		model.addAttribute("pageSize", lstSEOs.getSize());
		return "mod_seo";
	}
	
	@RequestMapping("/seo/update-url")
	public String getUpdateUrlSEOInfo(@RequestParam String url, Model model){
		
		SEO seo = seoService.findOne(url);
		
		if(seo == null){
			
			seo = new SEO(url);
		}
		
		model.addAttribute("seo", seo);
		return "mod_seo_update_url";
	}
	
	@RequestMapping(value = "/seo/update-url", method = RequestMethod.POST)
	public String updateUrlSEOInfo(@Valid @ModelAttribute("seo") SEO seo, BindingResult results, Model model, RedirectAttributes redirectAttributes){
		
		if(results.hasErrors()){
			
			return "mod_seo_update_url";
		}
		
		SEO dbSEO = seoService.findOne(seo.getUrl());
		
		if(dbSEO != null && dbSEO.getId() != seo.getId()){
			
			results.rejectValue("url", "exist");
			return "mod_seo_update_url";
		}
		
		seoService.save(seo);
		redirectAttributes.addFlashAttribute("flag", 1);
		return "redirect:../seos";
	}
	
	@RequestMapping("/seo/delete-url")
	public String deleteUrlSEO(@RequestParam String url, Model model){
		
		SEO seo = seoService.findOne(url);
		if(seo != null){
		
			seoService.delete(seo);
		}
		
		return "redirect:../seos";
	}
	
	@RequestMapping("/seo/delete")
	public String deleteSEO(@RequestParam Integer id, Model model){
		
		SEO seo = seoService.findOne(id);
		if(seo != null){
		
			seoService.delete(seo);
		}
		
		return "redirect:../seos";
	}
	
	@RequestMapping("/seo/update")
	public String getUpdateSEOInfo(@RequestParam Integer id,
			@RequestParam Integer typeId, Model model){
		
		SEO seo = null;
		
		if(id == null){
			
			seo = new SEO();
		}else{

			seo = seoService.findOne(id);
		}
		
		if(seo == null){
			
			seo = new SEO();
		}
		
		model.addAttribute("typeId", typeId);
		model.addAttribute("seo", seo);
		return "mod_seo_update";
	}
	
	@RequestMapping(value = "/seo/update", method = RequestMethod.POST)
	public String updateSEOInfo(@RequestParam Integer typeId, @Valid @ModelAttribute("seo") SEO seo, 
			BindingResult results, Model model, RedirectAttributes redirectAttributes){
		
		if(results.hasErrors()){
			
			return "mod_seo_update";
		}
		
		SEO dbSEO = seoService.findOne(seo.getId());
		
		if(dbSEO != null && dbSEO.getId() != seo.getId()){
			
			results.rejectValue("url", "exist");
			return "mod_seo_update";
		}
		String image = "";
		
		switch(seo.getType()){
			case PAGE:{
				
				break;
			}
			case POST:{
				
				Post post = postService.findOne(typeId);
				image = post.getImage();
				seo.setPost(post);
				break;
			}
			case CATEGORY:{
				
				Category category = categoryService.findOne(typeId);
				image = category.getImage();
				seo.setCategory(category);
				break;
			}
			case TAG:{
				
				break;
			}
			case USER:{
				
				break;
			}
			default:{
				
			}
		}
		// generating timestamp for url
		seo.setUrl(String.valueOf(System.currentTimeMillis()));
		if("".equals(seo.getImage()) && !"".equals(image)){
			seo.setImage(image);
		}
		
		seoService.save(seo);
		redirectAttributes.addFlashAttribute("flag", 1);
		return "redirect:../seos";
	}
}
