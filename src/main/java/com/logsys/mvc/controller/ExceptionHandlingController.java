package com.logsys.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ControllerAdvice
public class ExceptionHandlingController {

	@ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleResourceNotFoundException(Exception ex) {

		return "redirect:/error/404";
	}
	
	// Total control - setup a model and return the view name yourself. Or
	// consider
	// subclassing ExceptionHandlerExceptionResolver (see below).
	@ExceptionHandler(Exception.class)
	public String handleError(HttpServletRequest req, Exception exception) {

		return "redirect:/error/404";
	}
}
