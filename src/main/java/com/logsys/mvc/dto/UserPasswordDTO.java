package com.logsys.mvc.dto;

import org.hibernate.validator.constraints.NotEmpty;

import com.logsys.mvc.model.User;

public class UserPasswordDTO {

	private String email;
	
	private String firstName;
	
	private String lastName;
	
	@NotEmpty
	private String password;
	
	@NotEmpty
	private String newPassword;
	
	@NotEmpty
	private String confirmPassword;
	
	public UserPasswordDTO(){
		
	}
	
	public UserPasswordDTO(User user){
		
		this.email = user.getEmail();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
