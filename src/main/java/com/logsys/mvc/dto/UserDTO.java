package com.logsys.mvc.dto;

import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.logsys.utils.enumerations.Sex;
import com.logsys.utils.enumerations.UserStatus;

public class UserDTO {

	private int id;

	@NotEmpty
	private String firstName;

	@NotEmpty
	private String lastName;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date birthday;

	@NotEmpty
	private String email;

	private String password;

	private Sex sex;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date createdDate;

	private UserStatus status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}
}
