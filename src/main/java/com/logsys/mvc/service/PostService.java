package com.logsys.mvc.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import com.logsys.mvc.dto.PostDTO;
import com.logsys.mvc.model.Post;
import com.logsys.utils.enumerations.PostStatus;

public interface PostService extends GenericService<Post>{
	
	@PostAuthorize("returnObject.author.email == authentication.name or hasRole('ADMIN')")
	public Post findOne(Serializable id);
	
	@PreAuthorize("#post.author.email == authentication.name or hasRole('ADMIN')")
	public Post save(Post post);
	
	@PreAuthorize("#post.author.email == authentication.name or hasRole('ADMIN')")
	public void delete(Post post);
	
	public Page<Post> findPostByStatus(PostStatus status, Pageable pageRequest);
	
	public Page<Post> findPostByCategory(Serializable id, PostStatus status, Pageable pageRequest);
	
	public List<Post> findTopPostByView(PostStatus status, Pageable pageRequest);
	
	public List<Post> findTopPostByLike(PostStatus status, Pageable pageRequest);
	
	public List<Post> findTopPostByShare(PostStatus status, Pageable pageRequest);
	
	public List<Post> findNewPost(PostStatus status, Pageable pageRequest);
	
	public List<Post> findRandomPost(PostStatus status, Pageable pageRequest);
	
	public Page<Post> findAllPost(Pageable pageRequest);
	
	Page<PostDTO> findPostByTitle(String userEmail, String title, Integer categoryId, PostStatus status, Pageable pageRequest);

	public Post findPostByAlias(String alias, PostStatus status);
	
	public Post findPostByAlias(String alias);
	
	public int countPostByCategory(Integer categoryId, PostStatus status);
	
	/**
	 * Moderator methods
	 */
	public Page<Post> findAllPost(String userEmail, Pageable pageRequest);
}
