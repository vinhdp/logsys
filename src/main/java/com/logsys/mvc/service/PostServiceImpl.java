package com.logsys.mvc.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.logsys.mvc.dao.PostRepository;
import com.logsys.mvc.dto.PostDTO;
import com.logsys.mvc.model.Post;
import com.logsys.utils.enumerations.PostStatus;

@Service("postService")
public class PostServiceImpl implements PostService {

	@Autowired
	private PostRepository postRepository;

	@Override
	public Post findOne(Serializable id) {

		return postRepository.findOne(id);
	}

	@Override
	public Iterable<Post> findAll() {

		return postRepository.findAll();
	}

	@Override
	public Iterable<Post> findAll(Iterable<Serializable> ids) {

		return postRepository.findAll(ids);
	}

	@Override
	public Post save(Post post) {

		return postRepository.save(post);
	}

	@Override
	public Iterable<Post> save(Iterable<Post> posts) {

		return postRepository.save(posts);
	}

	@Override
	public void delete(Serializable id) {

		postRepository.delete(id);
	}

	@Override
	public void delete(Post post) {

		postRepository.delete(post);
	}

	@Override
	public void delete(Iterable<Post> posts) {

		postRepository.delete(posts);
	}

	@Override
	public void deleteAll() {

		postRepository.deleteAll();
	}

	@Override
	public Boolean exists(Serializable id) {

		return postRepository.exists(id);
	}

	@Override
	public Page<Post> findPostByStatus(PostStatus status, Pageable pageRequest) {

		return postRepository.findPostByStatus(status, pageRequest);
	}

	@Override
	public Page<Post> findPostByCategory(Serializable id, PostStatus status, Pageable pageRequest) {

		return postRepository.findPostByCategory(id, status, pageRequest);
	}

	@Override
	public List<Post> findNewPost(PostStatus status, Pageable pageRequest) {

		return postRepository.findNewPost(status, pageRequest);
	}

	@Override
	public List<Post> findTopPostByView(PostStatus status, Pageable pageRequest) {

		return postRepository.findTopPostByView(status, pageRequest);
	}

	@Override
	public List<Post> findTopPostByLike(PostStatus status, Pageable pageRequest) {

		return postRepository.findTopPostByLike(status, pageRequest);
	}

	@Override
	public List<Post> findTopPostByShare(PostStatus status, Pageable pageRequest) {

		return postRepository.findTopPostByShare(status, pageRequest);
	}

	@Override
	public List<Post> findRandomPost(PostStatus status, Pageable pageRequest) {

		return postRepository.findRandomPost(status, pageRequest);
	}

	@Override
	public Page<Post> findAllPost(Pageable pageRequest) {

		return postRepository.findAllPost(pageRequest);
	}

	@Override
	public Page<PostDTO> findPostByTitle(String userEmail, String title, Integer categoryId, PostStatus status, Pageable pageRequest) {

		return postRepository.findPostByTitle(userEmail, title, categoryId, status, pageRequest);
	}

	@Override
	public Post findPostByAlias(String alias, PostStatus status) {

		return postRepository.findPostByAlias(alias, status);
	}

	@Override
	public Post findPostByAlias(String alias) {

		return postRepository.findPostByAlias(alias);
	}

	@Override
	public int countPostByCategory(Integer categoryId, PostStatus status) {

		return postRepository.countPostByCategory(categoryId, status);
	}

	@Override
	public Page<Post> findAllPost(String userEmail, Pageable pageRequest) {

		return postRepository.findAllPost(userEmail, pageRequest);
	}
}
