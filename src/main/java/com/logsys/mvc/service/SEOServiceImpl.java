package com.logsys.mvc.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.logsys.mvc.dao.SEORepository;
import com.logsys.mvc.model.SEO;
import com.logsys.utils.enumerations.SEOType;

@Service("seoService")
public class SEOServiceImpl implements SEOService{

	@Autowired
	private SEORepository seoRepository;
	
	@Override
	public SEO findOne(Serializable id) {

		return seoRepository.findOne(id);
	}

	@Override
	public Iterable<SEO> findAll() {

		return seoRepository.findAll();
	}

	@Override
	public Iterable<SEO> findAll(Iterable<Serializable> ids) {

		return seoRepository.findAll(ids);
	}

	@Override
	public SEO save(SEO entity) {

		return seoRepository.save(entity);
	}

	@Override
	public Iterable<SEO> save(Iterable<SEO> entities) {

		return seoRepository.save(entities);
	}

	@Override
	public void delete(Serializable id) {

		seoRepository.delete(id);
	}

	@Override
	public void delete(SEO entity) {

		seoRepository.delete(entity);
	}

	@Override
	public void delete(Iterable<SEO> entities) {

		seoRepository.delete(entities);
	}

	@Override
	public void deleteAll() {

		seoRepository.deleteAll();
	}

	@Override
	public Boolean exists(Serializable id) {

		return seoRepository.exists(id);
	}

	@Override
	public SEO findOne(String url) {

		return seoRepository.findOne(url);
	}

	@Override
	public Page<SEO> findAll(Pageable pageRequest) {

		return seoRepository.findAll(pageRequest);
	}

	@Override
	public Page<SEO> findSEOByType(SEOType type, Pageable pageRequest) {

		return seoRepository.findSEOByType(type, pageRequest);
	}

	@Override
	public boolean isExisted(Serializable id, String url) {

		return seoRepository.isExisted(id, url);
	}
}
