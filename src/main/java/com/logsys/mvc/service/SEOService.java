package com.logsys.mvc.service;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import com.logsys.mvc.model.SEO;
import com.logsys.utils.enumerations.SEOType;

public interface SEOService extends GenericService<SEO> {

	//@PostAuthorize("(returnObject!=null?returnObject.post.author.email == authentication.name:true) or hasRole('ADMIN')")
	public SEO findOne(String url);
	
	//@PostAuthorize("(returnObject!=null?returnObject.post.author.email == authentication.name:true) or hasRole('ADMIN')")
	public SEO findOne(Serializable id);
	
	//@PreAuthorize("#seo.post.author.email == authentication.name or hasRole('ADMIN')")
	public SEO save(SEO seo);
	
	//@PreAuthorize("#seo.post.author.email == authentication.name or hasRole('ADMIN')")
	public void delete(SEO seo);
	
	public Page<SEO> findAll(Pageable pageRequest);
	
	public Page<SEO> findSEOByType(SEOType type, Pageable pageRequest);
	
	public boolean isExisted(Serializable id, String url);
}
