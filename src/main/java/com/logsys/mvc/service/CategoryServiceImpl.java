package com.logsys.mvc.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.logsys.mvc.dao.CategoryRepository;
import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.utils.enumerations.CategoryStatus;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Transactional(readOnly = true)
	@Override
	public Category findOne(Serializable id) {

		return categoryRepository.findOne(id);
	}

	@Transactional(readOnly = true)
	@Override
	public Iterable<Category> findAll() {

		return categoryRepository.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Iterable<Category> findAll(Iterable<Serializable> ids) {

		return categoryRepository.findAll(ids);
	}

	@Transactional
	@Override
	public Category save(Category category) {

		return categoryRepository.save(category);
	}

	@Transactional
	@Override
	public Iterable<Category> save(Iterable<Category> categories) {

		return categoryRepository.save(categories);
	}

	@Transactional
	@Override
	public void delete(Serializable id) {
		
		categoryRepository.delete(id);
	}

	@Transactional
	@Override
	public void delete(Category category) {
		
		categoryRepository.delete(category);
	}

	@Transactional
	@Override
	public void delete(Iterable<Category> categories) {
		
		categoryRepository.delete(categories);
	}

	@Transactional
	@Override
	public void deleteAll() {
		
		categoryRepository.deleteAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Boolean exists(Serializable id) {
		
		return categoryRepository.exists(id);
	}

	@Transactional(readOnly = true)
	@Override
	public Iterable<Category> findMenuCategory() {

		Iterable<Category> mainCategories = categoryRepository.findMenuCategory();
		
		for(Category parent : mainCategories){
			
			List<Category> subCategories = Lists.newArrayList(categoryRepository.findShowingSubCategory(parent.getId()));
			parent.setSubCategories(subCategories);
		}
		
		return mainCategories;
	}

	@Transactional(readOnly = true)
	@Override
	public Iterable<Category> findShowingSubCategory(Serializable id) {

		return categoryRepository.findShowingSubCategory(id);
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Category> findSubCategory(Serializable id, CategoryStatus status, Pageable pageRequest) {

		return categoryRepository.findSubCategory(id, status, pageRequest);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Page<Category> findSubCategory(Serializable id, Pageable pageRequest) {

		return categoryRepository.findSubCategory(id, pageRequest);
	}

	@Transactional(readOnly = true)
	@Override
	public Iterable<Category> findCategoryAncestors(Serializable id) {

		List<Category> lstAncestors = new ArrayList<Category>();
		Category category = categoryRepository.findOne(id);
		lstAncestors.add(category);
		
		Category superCategory = category.getSuperCategory();
		while(superCategory != null){
			
			lstAncestors.add(superCategory);
			superCategory = superCategory.getSuperCategory();
		}
		
		Collections.reverse(lstAncestors);
		return lstAncestors;
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Category> findCategory(CategoryStatus status, Pageable pageRequest) {

		return categoryRepository.findCategory(status, pageRequest);
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Category> findSubCategoryRest(Serializable id, Pageable pageRequest) {

		return categoryRepository.findSubCategoryRest(id, pageRequest);
	}

	@Override
	public List<Post> findRandomPost(Serializable id, Pageable pageRequest) {

		
		return categoryRepository.findRandomPost(id, pageRequest);
	}

	@Override
	public Category findOneByAlias(String alias) {

		return categoryRepository.findOneByAlias(alias);
	}

	@Override
	public Category findOneByAlias(String alias, CategoryStatus status) {

		return categoryRepository.findOneByAlias(alias, status);
	}
}
