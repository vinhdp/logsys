package com.logsys.mvc.service;

import java.io.Serializable;

public interface GenericService<T> {

	public T findOne(Serializable id);

	public Iterable<T> findAll();

	public Iterable<T> findAll(Iterable<Serializable> ids);

	public T save(T entity);
	
	public Iterable<T> save(Iterable<T> entities);

	public void delete(Serializable id);

	public void delete(T entity);

	public void delete(Iterable<T> entities);

	public void deleteAll();

	public Boolean exists(Serializable id);

}
