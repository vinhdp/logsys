package com.logsys.mvc.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.logsys.mvc.model.User;
import com.logsys.utils.enumerations.UserStatus;

public interface UserService extends GenericService<User>, UserDetailsService {

	public Page<User> findUserByStatus(UserStatus status, Pageable pageRequest);
	
	public Page<User> findAllUsers(Pageable pageRequest);
	
	public User findUserByEmail(String email);
}