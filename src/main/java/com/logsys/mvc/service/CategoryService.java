package com.logsys.mvc.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.model.Post;
import com.logsys.utils.enumerations.CategoryStatus;

public interface CategoryService extends GenericService<Category> {

	/*public Category findOne(Serializable id);
	
	public Iterable<Category> findAll();
	
	public Iterable<Category> findAll(Iterable<Serializable> ids);
	
	public Category save(Category category);
	
	public Iterable<Category> save(Iterable<Category> categories);
	
	public void delete(Serializable id);
	
	public void delete(Category category);
	
	public void delete(Iterable<Category> categories);
	
	public void deleteAll();
	
	public Boolean exists(Serializable id);
	*/
	/* Additional methods */
	
	public Iterable<Category> findMenuCategory();
	
	public Iterable<Category> findShowingSubCategory(Serializable id);
	
	public Page<Category> findSubCategory(Serializable id, CategoryStatus status, Pageable pageRequest);
	
	public Page<Category> findSubCategory(Serializable id, Pageable pageRequest);
	
	public Iterable<Category> findCategoryAncestors(Serializable id);
	
	public Page<Category> findCategory(CategoryStatus status, Pageable pageRequest);
	
	public Page<Category> findSubCategoryRest(Serializable id, Pageable pageRequest);
	
	public List<Post> findRandomPost(Serializable id, Pageable pageRequest);
	
	public Category findOneByAlias(String alias);
	
	public Category findOneByAlias(String alias, CategoryStatus status);
}
