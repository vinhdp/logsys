package com.logsys.mvc.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logsys.mvc.dao.UserRepository;
import com.logsys.mvc.model.User;
import com.logsys.mvc.model.UserRole;
import com.logsys.utils.enumerations.UserStatus;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User findOne(Serializable id) {

		return userRepository.findOne(id);
	}

	@Override
	public Iterable<User> findAll() {

		return userRepository.findAll();
	}

	@Override
	public Iterable<User> findAll(Iterable<Serializable> ids) {

		return userRepository.findAll(ids);
	}

	@Override
	public User save(User user) {

		return userRepository.save(user);
	}

	@Override
	public Iterable<User> save(Iterable<User> users) {

		return userRepository.save(users);
	}

	@Override
	public void delete(Serializable id) {

		userRepository.delete(id);
	}

	@Override
	public void delete(User user) {

		userRepository.delete(user);
	}

	@Override
	public void delete(Iterable<User> users) {

		userRepository.delete(users);
	}

	@Override
	public void deleteAll() {

		userRepository.deleteAll();
	}

	@Override
	public Boolean exists(Serializable id) {

		return userRepository.exists(id);
	}

	@Override
	public Page<User> findUserByStatus(UserStatus status, Pageable pageRequest) {

		return userRepository.findUserByStatus(status, pageRequest);
	}

	@Override
	public Page<User> findAllUsers(Pageable pageRequest) {

		return userRepository.findAllUsers(pageRequest);
	}

	// Converts com.mkyong.users.model.User user to
	// org.springframework.security.core.userdetails.User
	private org.springframework.security.core.userdetails.User buildUserForAuthentication(
			com.logsys.mvc.model.User user, List<GrantedAuthority> authorities) {
		
		boolean enable = false;
		boolean accountNonExpire = true;
		boolean credentialsNonExpire = true;
		boolean accountNonLocked = true;
		
		if(UserStatus.ACTIVE.equals(user.getStatus())){
			
			enable = true;
		}
		
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), enable, accountNonExpire,
				credentialsNonExpire, accountNonLocked, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);

		return result;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
		
		User user = userRepository.findUserByEmail(email);
		List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());
		return buildUserForAuthentication(user, authorities);
	}

	@Override
	public User findUserByEmail(String email) {

		return userRepository.findUserByEmail(email);
	}
}
