package com.logsys.mvc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logsys.utils.enumerations.PostStatus;

@Entity
@Table(name = "posts")
@DynamicUpdate(value = true)
public class Post implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "post_id")
	private int id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "alias", unique = true)
	private String alias;
	
	@Lob
	@Column(name = "content")
	private String content;
	
	@Column(name = "keyword")
	private String keyword;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "status")
	private PostStatus status;
	
	@Column(name = "viewed")
	private int view;
	
	@Column(name = "liked")
	private int like;
	
	@Column(name = "shared")
	private int share;
	
	@Column(name = "image",
			columnDefinition = "varchar(255) default '/resources/themes/img/blog.png'")
	private String image;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "published_date")
	private Date publishedDate;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category_id", nullable = false)
	private Category category;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "author_id", nullable = false, updatable = false)
	private User author;
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "post")
	@JsonIgnore
	private SEO seo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PostStatus getStatus() {
		return status;
	}

	public void setStatus(PostStatus status) {
		this.status = status;
	}

	public int getView() {
		return view;
	}

	public void setView(int view) {
		this.view = view;
	}

	public int getLike() {
		return like;
	}

	public void setLike(int like) {
		this.like = like;
	}

	public int getShare() {
		return share;
	}

	public void setShare(int share) {
		this.share = share;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		if("".equals(image)){
			this.image = "/resources/themes/img/blog.png";
		}else{
			this.image = image;
		}
	}

	/** Relationship setter/getter */
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public SEO getSeo() {
		return seo;
	}

	public void setSeo(SEO seo) {
		this.seo = seo;
	}
}
