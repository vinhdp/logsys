package com.logsys.mvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logsys.utils.enumerations.CategoryStatus;

@Entity
@Table(name = "categories")
public class Category implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Category(){
		
	}

	public Category(String name, String title, String alias, String keyword, String description, int element,
			String image, int menuOrder, int parentOrder, CategoryStatus status) {
		super();
		this.name = name;
		this.title = title;
		this.alias = alias;
		this.keyword = keyword;
		this.description = description;
		this.element = element;
		this.image = image;
		this.menuOrder = menuOrder;
		this.parentOrder = parentOrder;
		this.status = status;
	}

	@Id
	@GeneratedValue
	@Column(name = "category_id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "title")
	private String title;

	@Column(name = "alias", unique = true)
	private String alias;

	@Column(name = "keyword")
	private String keyword;

	@Column(name = "description")
	private String description;

	@Column(name = "element")
	private int element;
	
	@Column(name = "image",
			columnDefinition = "varchar(255) default '/resources/themes/img/category.png'")
	
	private String image;
	
	@Column(name = "menu_order", columnDefinition = "int default 0")
	private int menuOrder;
	
	@Column(name = "parent_order", columnDefinition = "int default 0")
	private int parentOrder;
	
	@Column(name = "status")
	private CategoryStatus status;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	private Set<Post> posts = new HashSet<Post>();

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "super_id", nullable = true)
	private Category superCategory;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "superCategory")
	private List<Category> subCategories = new ArrayList<Category>();

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "category_moderator", joinColumns = { @JoinColumn(name = "category_id") }, inverseJoinColumns = {
			@JoinColumn(name = "moderator_id") })
	private Set<User> moderators = new HashSet<User>();
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "category")
	private SEO seo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getElement() {
		return element;
	}

	public void setElement(int element) {
		this.element = element;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		if("".equals(image)){
			this.image = "/resources/themes/img/category.png";
		}else{
			this.image = image;
		}
	}

	public int getMenuOrder() {
		return menuOrder;
	}

	public void setMenuOrder(int menuOrder) {
		this.menuOrder = menuOrder;
	}

	public int getParentOrder() {
		return parentOrder;
	}

	public void setParentOrder(int parentOrder) {
		this.parentOrder = parentOrder;
	}

	public CategoryStatus getStatus() {
		return status;
	}

	public void setStatus(CategoryStatus status) {
		this.status = status;
	}

	/** Relationship setter/getter */
	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public Category getSuperCategory() {
		return superCategory;
	}

	public void setSuperCategory(Category superCategory) {
		this.superCategory = superCategory;
	}

	public List<Category> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<Category> subCategories) {
		this.subCategories = subCategories;
	}

	public Set<User> getModerators() {
		return moderators;
	}

	public void setModerators(Set<User> moderators) {
		this.moderators = moderators;
	}

	public SEO getSeo() {
		return seo;
	}

	public void setSeo(SEO seo) {
		this.seo = seo;
	}
}
