package com.logsys.mvc.model.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.service.CategoryService;

public class CategoryFormatter implements Formatter<Category>{

	@Autowired
	CategoryService categoryService;
	
	@Override
	public String print(Category category, Locale locale) {

		return category.getName();
	}

	@Override
	public Category parse(String id, Locale local) throws ParseException {

		return categoryService.findOne(id);
	}

}
