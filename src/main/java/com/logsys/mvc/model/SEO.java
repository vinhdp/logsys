package com.logsys.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.logsys.utils.enumerations.SEOType;

@Entity
@Table(name = "seo_info")
public class SEO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(unique = true, nullable = true)
	private String url;
	@NotEmpty
	private String title;
	@NotEmpty
	private String keyword;
	@NotEmpty
	private String description;
	
	@Column(name="image",
			columnDefinition = "varchar(255) default '/resources/themes/img/default-image.png'")
	private String image;
	
	private SEOType type;
	
	@OneToOne
	@JoinColumn(name = "post_id", nullable = true, updatable = false)
	private Post post;
	
	@OneToOne
	@JoinColumn(name = "category_id", nullable = true, updatable = false)
	private Category category;
	
	public SEO(){
		
	}
	
	public SEO(String title, String desc, String key){
		
		this.title = title;
		this.description = desc;
		this.keyword = key;
	}
	
	public SEO(String url){
		
		this.url = url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SEOType getType() {
		return type;
	}

	public void setType(SEOType type) {
		this.type = type;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		if("".equals(image)){
			this.image = "/resources/themes/img/default-image.png";
		}else{
			this.image = image;
		}
	}
}
