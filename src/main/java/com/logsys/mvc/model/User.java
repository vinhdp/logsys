package com.logsys.mvc.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logsys.utils.enumerations.Sex;
import com.logsys.utils.enumerations.UserStatus;

@Entity
@Table(name = "users")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private int id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "birthday")
	private Date birthday;

	@Column(name = "email", unique = true)
	private String email;

	@JsonIgnore
	@Column(name = "password", columnDefinition = "varchar(255) default '$2a$10$8qAEQGjEJTonM8OWpnaNDef.zIFzxFrTxT8dy8XR.ox5QlBONxCYy'")
	private String password;

	@Column(name = "sex")
	private Sex sex;

	@Column(name = "create_date")
	private Date createdDate;

	@Column(name = "status")
	private UserStatus status;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
	private Set<Post> posts = new HashSet<Post>();

	@JsonIgnore
	@ManyToMany(mappedBy = "moderators")
	private Set<Category> manageCategories = new HashSet<Category>();

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserRole> userRole = new HashSet<UserRole>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	/** Relationship setter/getter */
	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public Set<Category> getManageCategories() {
		return manageCategories;
	}

	public void setManageCategories(Set<Category> manageCategories) {
		this.manageCategories = manageCategories;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}
}
