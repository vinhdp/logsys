package com.logsys.mvc.tiles.viewpreparer;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.PostService;
import com.logsys.utils.enumerations.PostStatus;

@Component
public class PageRelatedContentViewPreparer implements ViewPreparer {

	@Autowired
	private PostService postService;
	
	@Override
	public void execute(Request req, AttributeContext attributeContext) {
		HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		
		List<Post> lstNewPosts = postService.findNewPost(PostStatus.PUBLISHED, new PageRequest(0, 6));
		List<Post> lstRandomPosts = postService.findRandomPost(PostStatus.PUBLISHED, new PageRequest(0, 6));
		
		attributeContext.putAttribute("lstNewPosts", new Attribute(lstNewPosts));
		attributeContext.putAttribute("lstRandomPosts", new Attribute(lstRandomPosts));
	}
}
