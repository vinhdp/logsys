package com.logsys.mvc.tiles.viewpreparer;

import javax.servlet.http.HttpServletRequest;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.logsys.mvc.model.Category;
import com.logsys.mvc.service.CategoryService;

@Component
public class HeaderNavigationViewPreparer implements ViewPreparer {

	@Autowired
	private CategoryService categoryService;
	
	@Override
	public void execute(Request req, AttributeContext attributeContext) {
		HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		
		Iterable<Category> lstCategories = categoryService.findMenuCategory();
		attributeContext.putAttribute("lstCategories", new Attribute(lstCategories));
	}
}
