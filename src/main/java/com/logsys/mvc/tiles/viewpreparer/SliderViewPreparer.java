package com.logsys.mvc.tiles.viewpreparer;

import java.util.List;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.logsys.mvc.model.Post;
import com.logsys.mvc.service.PostService;
import com.logsys.utils.enumerations.PostStatus;

@Component
public class SliderViewPreparer implements ViewPreparer {

	@Autowired
	private PostService postService;
	
	@Override
	public void execute(Request req, AttributeContext attributeContext) {

		List<Post> lstNewPosts = postService.findNewPost(PostStatus.PUBLISHED, new PageRequest(0, 4));
		attributeContext.putAttribute("lstNewPosts", new Attribute(lstNewPosts));
	}
}
