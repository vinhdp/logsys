package com.logsys.mvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.SmartView;
import org.springframework.web.servlet.View;
import static org.springframework.web.servlet.view.UrlBasedViewResolver.REDIRECT_URL_PREFIX; 

import com.logsys.mvc.model.SEO;
import com.logsys.mvc.service.SEOService;

@Component
public class SEOInterceptor implements HandlerInterceptor{

	@Autowired  @Qualifier("seoService")
	private SEOService seoService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		boolean isRedirectView = isRedirectView(modelAndView);
		
		if(modelAndView != null && !isRedirectView) {
			
			String fullURL = request.getRequestURL().toString();
			String seoURL = fullURL.replace("http://localhost:8080/logsys", "");
			SEO seoInfo = null;
			
			if(fullURL.contains("/itechs") || fullURL.contains("/inotes")){
				
				seoInfo = new SEO("Title", "Description", "Keywords");
				modelAndView.addObject("title", seoInfo.getTitle());
				modelAndView.addObject("description", seoInfo.getDescription());
				modelAndView.addObject("keyword", seoInfo.getKeyword());
				modelAndView.addObject("image", seoInfo.getImage());
				modelAndView.addObject("url", request.getRequestURL().toString());
				return;
			}

			if(modelAndView.getModelMap().containsKey("seoInfo")){
				
				seoInfo = (SEO) modelAndView.getModel().get("seoInfo");
			}else{
				
				seoInfo = seoService.findOne(seoURL);
			}
			
			if(seoInfo == null){
				
				seoInfo = seoService.findOne("/seo-default");
				seoInfo = (seoInfo == null) ?  new SEO("Title", "Description", "Keywords") : seoInfo;
			}

			modelAndView.addObject("title", seoInfo.getTitle());
			modelAndView.addObject("description", seoInfo.getDescription());
			modelAndView.addObject("keyword", seoInfo.getKeyword());
			modelAndView.addObject("image", seoInfo.getImage());
			modelAndView.addObject("url", request.getRequestURL().toString());
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		//System.out.println("after completion");
	}

	private boolean isRedirectView(ModelAndView mv) {

		if(mv == null){
			
			// do not excute SEO
			return true;
		}
		
	    String viewName = mv.getViewName();
	    if (viewName.startsWith(REDIRECT_URL_PREFIX)) {
	        return true;
	    }

	    View view = mv.getView();
	    return (view != null && view instanceof SmartView
	            && ((SmartView) view).isRedirectView());
	}
}
