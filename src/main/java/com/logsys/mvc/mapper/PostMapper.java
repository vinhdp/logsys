package com.logsys.mvc.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.logsys.mvc.dto.PostDTO;
import com.logsys.mvc.model.Post;

@Mapper
public interface PostMapper {

	PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);

	@Mappings({
		@Mapping(source = "id", target = "id"),
	})
	Post postDTOToPost(PostDTO postDTO);

	@Mappings({
		@Mapping(source = "id", target = "id"),
	})
	PostDTO postToPostDTO(Post post);
	
	List<PostDTO> postsToPostDTOs(List<Post> listPosts);
}
