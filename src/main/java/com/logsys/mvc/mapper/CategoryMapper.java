package com.logsys.mvc.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.logsys.mvc.dto.CategoryDTO;
import com.logsys.mvc.model.Category;

@Mapper
public interface CategoryMapper {

	CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);
	
	@Mappings({
		@Mapping(source = "id", target = "id"),
	})
	Category categoryDTOToCategory(CategoryDTO categoryDTO);
	
	CategoryDTO categoryToCategoryDTO(Category category);
}
