package com.logsys.mvc.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.logsys.mvc.dto.UserDTO;
import com.logsys.mvc.model.User;

@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	@Mappings({
		@Mapping(source = "id", target = "id"),
	})
	User userDTOToUser(UserDTO userDTO);

	@Mappings({
		@Mapping(source = "id", target = "id"),
	})
	UserDTO userToUserDTO(User user);
}
