package com.logsys.utils.enumerations;

public enum PostStatus {

	/* Post Status */
	NEW, PUBLISHED, EDITING, REMOVED
}
