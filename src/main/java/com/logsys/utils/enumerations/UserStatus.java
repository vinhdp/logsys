package com.logsys.utils.enumerations;

public enum UserStatus {

	/* User Status */
	NOT_VERIFIED, ACTIVE, INACTIVE, BANNED, BLOCKED
}
