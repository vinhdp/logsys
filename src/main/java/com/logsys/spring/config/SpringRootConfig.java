/**
 * This is replacement class for rootConfig.xml
 * @author vinhdp
 * @version 1.0
 * @since 2016-01-11 
 */
package com.logsys.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({ "com.logsys.mvc" })
@PropertySource("classpath:application.properties")
public class SpringRootConfig {

}
