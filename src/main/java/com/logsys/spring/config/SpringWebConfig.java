/**
 * This is the replacement class for webConfig.xml
 * @author vinhdp
 * @version 1.0
 * @since 2016-01-11
 */
package com.logsys.spring.config;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.SpringBeanPreparerFactory;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import com.logsys.mvc.interceptor.SEOInterceptor;

@Configuration
@EnableWebMvc // <mvc:annotation-driven />
@ComponentScan({ "com.logsys.mvc", "com.logsys.spring" })
public class SpringWebConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

	@PostConstruct
	public void init() {

		// prevent model attribute appear on url
		requestMappingHandlerAdapter.setIgnoreDefaultModelOnRedirect(true);
	}

	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {

		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
		
		registry.addResourceHandler("/google4a8ef1d86113f200.html")
				.addResourceLocations("/google4a8ef1d86113f200.html").setCachePeriod(864000);
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/").setCachePeriod(864000);
		registry.addResourceHandler("/statics/**").addResourceLocations("file:/home/ec2-user/itechnotes/").setCachePeriod(864000);
		registry.addResourceHandler("/ckfinder/**").addResourceLocations("/resources/components/ckfinder/");
		registry.addResourceHandler("/*.xml").addResourceLocations("/");
		registry.addResourceHandler("/robots.txt").addResourceLocations("/robots.txt");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {

		configurer.enable();
	}

	@Bean
	public UrlBasedViewResolver viewResolver() {
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(TilesView.class);
		return viewResolver;
	}

	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions("/WEB-INF/tiles/layouts/layouts.xml", "/WEB-INF/tiles/preparer/preparer.xml",
				"/WEB-INF/tiles/views/users.xml", "/WEB-INF/tiles/views/members.xml", "/WEB-INF/tiles/views/mods.xml",
				"/WEB-INF/tiles/views/admins.xml", "/WEB-INF/tiles/views/globals.xml");
		tilesConfigurer.setValidateDefinitions(true);
		tilesConfigurer.setCheckRefresh(true);
		tilesConfigurer.setPreparerFactoryClass(SpringBeanPreparerFactory.class);
		return tilesConfigurer;
	}

	@Bean(name = "messageSource")
	public MessageSource messageSource() {

		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames("classpath:i18n/messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
		resolver.setMaxPageSize(20);
		resolver.setOneIndexedParameters(true);
		argumentResolvers.add(resolver);
		super.addArgumentResolvers(argumentResolvers);
	}

	@Bean
	public SEOInterceptor seoInterceptor() {

		return new SEOInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(seoInterceptor());
	}
}