package com.logsys.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	@Qualifier("userService")
	UserDetailsService userDetailsService;
	
	@Autowired
	public void globalConfiguration(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web
	      .ignoring()
	         .antMatchers("/resources/**")
	         .antMatchers("/ckfinder/**")
	         .antMatchers("/statics/**")
	         .antMatchers("/google4a8ef1d86113f200.html")
	         .antMatchers("/*.xml")
	         .antMatchers("/robots.txt");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter, CsrfFilter.class);
		
		http.headers().disable().authorizeRequests()
		.antMatchers("/inotes/login").permitAll()
		.antMatchers("/inotes/**").access("hasRole('ROLE_ADMIN')")
		.antMatchers("/mod/**").access("hasRole('ROLE_MOD')")
		.and().formLogin().loginPage("/login").defaultSuccessUrl("/inotes/")
		.usernameParameter("email").passwordParameter("password")
		.and().logout().logoutUrl("/logout").logoutSuccessUrl("/login?logout").deleteCookies("JSESSIONID")
		.and().csrf()
		.and().exceptionHandling().accessDeniedPage("/error/404");
	}
	
	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
}
