/**
 * This is the replacement class for web.xml
 * @author vinhdp
 * @version 1.0
 */
package com.logsys.spring.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.apache.tiles.extras.complete.CompleteAutoloadTilesListener;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import com.ckfinder.connector.ConnectorServlet;
import com.ckfinder.connector.FileUploadFilter;

public class WebAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {

		AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
		appContext.register(SpringWebConfig.class, SpringRootConfig.class, PersistenceContext.class,
				SpringSecurityConfig.class);
		appContext.setServletContext(servletContext);

		servletContext.addListener(new CompleteAutoloadTilesListener());

		Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(appContext));
		servlet.addMapping("/");
		servlet.setLoadOnStartup(1);
		
		Dynamic connectorServlet = servletContext.addServlet("connectorServlet", new ConnectorServlet());
		connectorServlet.addMapping("/ckfinder/core/connector/java/connector.java");
		connectorServlet.setInitParameter("XMLConfig", "/WEB-INF/config.xml");
		connectorServlet.setInitParameter("debug", "true");
		connectorServlet.setLoadOnStartup(2);
		
		FilterRegistration.Dynamic fileUploadFilter = servletContext.addFilter("fileUploadFilter", FileUploadFilter.class);
		fileUploadFilter.setInitParameter("sessionCookieName", "JSESSIONID");
		fileUploadFilter.setInitParameter("sessionParameterName", "jsessionid");
		fileUploadFilter.addMappingForServletNames(EnumSet.of(DispatcherType.REQUEST), true, "dispatcher");
		
		FilterRegistration.Dynamic filter = servletContext.addFilter("openEntityManagerInViewFilter",
				OpenEntityManagerInViewFilter.class);
		filter.setInitParameter("singleSession", "true");
		filter.addMappingForServletNames(EnumSet.of(DispatcherType.REQUEST), true, "dispatcher");
		
		EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);
		
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);

        FilterRegistration.Dynamic characterEncoding = servletContext.addFilter("characterEncoding", characterEncodingFilter);
        characterEncoding.addMappingForUrlPatterns(dispatcherTypes, true, "/*");
        characterEncoding.setAsyncSupported(true);
		
		/**
	     * Add Spring ContextLoaderListener
	     * Need to use OpenEntityManagerInViewFilter
	     */
	    servletContext.addListener(new ContextLoaderListener(appContext));
	}
}
