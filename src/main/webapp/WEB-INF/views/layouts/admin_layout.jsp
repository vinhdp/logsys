<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="context" value="${pageContext.request.servletContext.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><tiles:insertAttribute name="title" ignore="false"></tiles:insertAttribute></title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<link rel='stylesheet' type='text/css'
	href='<spring:url value="/resources/components/bootstrap/css/bootstrap.min.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/font-awesome/css/font-awesome.min.css"/>' />
<!-- jvectormap -->
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/adminlte2/plugins/jvectormap/jquery-jvectormap-1.2.2.css"/>'>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<tiles:importAttribute name="styles" />
<c:forEach var="style" items="${styles}">
	<link rel="stylesheet" type="text/css"
		href="${context}/resources/${style}" />
</c:forEach>
<!-- Theme style -->
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/adminlte2/css/AdminLTE.min.css"/>'>
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/adminlte2/css/skins/_all-skins.min.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/themes/css/admin/stylesheet.css"/>'>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<tiles:insertAttribute name="header_main"></tiles:insertAttribute>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<tiles:insertAttribute name="sidebar_main"></tiles:insertAttribute>
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<tiles:insertAttribute name="content_header"></tiles:insertAttribute>
			</section>
			<!-- Main content -->
			<section class="content">
				<tiles:insertAttribute name="content"></tiles:insertAttribute>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<tiles:insertAttribute name="footer"></tiles:insertAttribute>
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<tiles:insertAttribute name="sidebar_control"></tiles:insertAttribute>
		</aside>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>

	</div>
	<!-- ./wrapper -->

	<script
		src="<spring:url value="/resources/components/jquery/jquery-2.2.0.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/bootstrap/js/bootstrap.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/plugins/fastclick/fastclick.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/plugins/sparkline/jquery.sparkline.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/plugins/slimScroll/jquery.slimscroll.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/plugins/chartjs/Chart.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/themes/js/global.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/adminlte2/js/app.min.js"/>"></script>
	<%-- <script
		src="<spring:url value="/resources/components/adminlte2/js/demo.js"/>"></script> --%>
	<script
		src="<spring:url value="/resources/themes/js/utils.js"/>"></script>
	<tiles:importAttribute name="scripts" />
	<c:forEach var="script" items="${scripts}">
		<script src="${context}/resources/${script}"></script>
	</c:forEach>
	<script type="text/javascript">
		$(".form-error").parent().addClass("has-error");
	</script>
	<script>
		$(function() {
			var pgurl = location.pathname;
			//pgurl.replace("/logsys", "");
			var prefix = ''; //[ location.protocol, '//',  location.host, '/', 'logsys'].join('');
			$("ul.sidebar-menu li a").each(
					function() {
						
						var itemUrl = $(this).attr("href").replace(prefix, "");

						if (itemUrl == pgurl || itemUrl == '/') {
							
							$(this).parent().addClass("active");
						} else {
							
							$(this).parent().removeClass("active");
						}
					})
		});
		
		/* $(document).ready(function() {
			  $('pre code').each(function(i, block) {
			    hljs.highlightBlock(block);
			  });
			}); */
	</script>
</body>
</html>
