<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="context"
	value="${pageContext.request.servletContext.contextPath}" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="iTechNotes.com">
<meta name="keywords" content="${keyword}">
<meta name="description" content="${description}">

<meta property="og:url"           content="${url}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="<tiles:insertAttribute name="title" ignore="false"></tiles:insertAttribute>" />
<meta property="og:description"   content="${description}" />

<meta property="fb:admins" content="100006008104437"/>

<c:choose>
	<c:when test="${image != null }">
		<meta property="og:image"         content="http://www.itechnotes.com${image}" />
	</c:when>
	<c:otherwise>
		<meta property="og:image"         content='http://www.itechnotes.com<spring:url value="/resources/themes/img/default-image.png" />' />
	</c:otherwise>
</c:choose>


<title><tiles:insertAttribute name="title" ignore="false"></tiles:insertAttribute></title>

<link rel="shortcut icon" type="image/x-icon"
	href="<spring:url value="/resources/themes/img/favicon.ico"/>" />
<link rel='stylesheet' type='text/css'
	href='<spring:url value="/resources/components/bootstrap/css/bootstrap.min.css"/>' />
<link rel='stylesheet' type='text/css'
	href='<spring:url value="/resources/components/bootstrap/css/bootstrap-theme.min.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/pushmenu/css/normalize.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/pushmenu/css/demo.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/pushmenu/css/icons.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/pushmenu/css/component.css"/>' />

<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/osvaldas.info/_normalize.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/font-awesome/css/font-awesome.min.css"/>' />
<link rel='stylesheet' type='text/css'
	href='<spring:url value="/resources/components/bootstrap-social/bootstrap-social.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/osvaldas.info/inline.css"/>' />
<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/components/swipeslider/css/style.css"/>' />

<link rel="stylesheet" type="text/css"
	href='<spring:url value="/resources/themes/css/stylesheet.css"/>' />
<tiles:importAttribute name="styles" />
<c:forEach var="style" items="${styles}">
	<link rel="stylesheet" type="text/css"
		href="${context}/resources/${style}" />
</c:forEach>

<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script> -->
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
</head>
<body>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=150522102034576";
			  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- Push Wrapper -->
	<div class="mp-pusher" id="mp-pusher">
		<tiles:insertAttribute name="header_push_menu"></tiles:insertAttribute>
		<div class="scroller">
			<!-- this is for emulating position fixed of the nav -->
			<div class="scroller-inner">

				<!-- Header -->
				<header class="codrops-header" id="top">
					<div class="codrops-top clearfix">
						<tiles:insertAttribute name="header_info"></tiles:insertAttribute>
					</div>
					<div class="codrops-search">
						<tiles:insertAttribute name="header_search"></tiles:insertAttribute>
					</div>
					<div class="codrops-nav">
						<tiles:insertAttribute name="header_nav"></tiles:insertAttribute>
					</div>
					<div class="codrops-panel">
						<tiles:insertAttribute name="header_slider"></tiles:insertAttribute>
					</div>
					<div class="codrops-submenu">
						<tiles:insertAttribute name="content_submenu"></tiles:insertAttribute>
					</div>
				</header>
				<div class="codrops-content">
					<div class="container">
						<div class="row">
							<tiles:insertAttribute name="content"></tiles:insertAttribute>
							<tiles:insertAttribute name="sidebar"></tiles:insertAttribute>
						</div>
						<div class="row related-content no-padding">
							<tiles:insertAttribute name="related_content"></tiles:insertAttribute>
						</div>
					</div>
				</div>
				<footer>
					<div class="codrops-footer">
						<tiles:insertAttribute name="footer"></tiles:insertAttribute>
					</div>
					<div class="codrops-copyright">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<b>Copyright © 2016-2017 itechnotes.com, all rights reserved.</b>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
			<!-- /scroller-inner -->
		</div>
		<!-- /scroller -->
	</div>
	<!-- /pusher -->

	<script
		src="<spring:url value="/resources/components/jquery/jquery-2.2.0.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/bootstrap/js/bootstrap.min.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/pushmenu/js/modernizr.custom.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/pushmenu/js/classie.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/pushmenu/js/mlpushmenu.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/osvaldas.info/doubletaptogo.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/momentjs/moment.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/momentjs/jquery.timeago.js"/>"></script>
	<script
		src="<spring:url value="/resources/themes/js/global.js"/>"></script>
	<script
		src="<spring:url value="/resources/components/osvaldas.info/main.js"/>"></script>
	<script>
		$(function() {
			var pgurl = location.pathname;
			//pgurl.replace("/logsys", "");
			var prefix = ''; //[ location.protocol, '//',  location.host, '/', 'logsys'].join('');
			$("#nav ul li a").each(
					function() {
						
						var itemUrl = $(this).attr("href").replace(prefix, "");

						if (itemUrl == pgurl)
							$(this).parent().addClass("active");
					})
		});
		
		/* $(document).ready(function() {
			  $('pre code').each(function(i, block) {
			    hljs.highlightBlock(block);
			  });
			}); */
	</script>
	<tiles:importAttribute name="scripts" />
	<c:forEach var="script" items="${scripts}">
		<script src="${context}/resources/${script}"></script>
	</c:forEach>
	<script type="text/javascript"
		src="<spring:url value="/resources/components/swipeslider/js/jssor.slider.mini.js"/>"></script>
	<script>
		new mlPushMenu(document.getElementById('mp-menu'), document
				.getElementById('trigger'));
	</script>
	<script>
		$(function() {
			$('#nav li:has(ul)').doubleTapToGo();
		});
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-82079160-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
</body>
</html>