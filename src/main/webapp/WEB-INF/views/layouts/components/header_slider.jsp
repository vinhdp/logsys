<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:importAttribute name="lstNewPosts"/>
<div class="container">
	<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('resources/components/swipeslider/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="resources/themes/img/java.jpg" alt="The most famous programming language!"/>
                <div style="position: absolute; top: 30px; left: 30px; width: 700px; height: 120px; font-size: 50px; color: #ffffff; line-height: 60px;">itechnotes.com<br>when you learn, teach<br>when you get, give</div>
                <div style="position: absolute; top: 300px; left: 30px; width: 700px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">failure is simply the opportunity to begin again</div>
            </div>
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="resources/themes/img/big-data.jpg" alt="Big data processing framework, solutions!"/>
            </div>
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="resources/themes/img/programming.jpg" alt="Useful programming notes!"/>
            </div>
            <c:forEach var="post" items="${lstNewPosts}">
            	<div data-p="225.00" style="display: none;">
	            	<c:choose>
	            		<c:when test="${post.id % 2 == 0}">
				            <img data-u="image" src="resources/themes/img/software-development.jpg" alt="Software deveplopment process, dev tools, framework!" />
	            		</c:when>
	            		<c:otherwise>
				            <img data-u="image" src="resources/themes/img/web-development.jpg"  alt="Web development framework!"/>
	            		</c:otherwise>
	            	</c:choose>
	            	<div style="position: absolute; top: 30px; left: 30px; width: 480px; height: 120px; font-size: 50px; color: #ffffff; line-height: 60px;">${post.title}</div>
				    <div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;" class="block-with-text">${post.description}</div>
            	</div>
            </c:forEach>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>

</div>