<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="container">
	<ul id="breadcrumb">
		<li><a href='<spring:url value="/"></spring:url>'><span class="fa fa-home"> </span></a></li>
		<c:forEach var="category" items="${lstAncestors}">
			<li>
				<a href='<spring:url value="/category/${category.alias}"></spring:url>'>
					${category.name}
				</a>
			</li>
		</c:forEach>
	</ul>
</div>