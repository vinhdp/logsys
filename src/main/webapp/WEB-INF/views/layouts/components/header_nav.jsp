<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:importAttribute name="lstCategories"/>
<!-- header navigation -->
<div class="container">
	<nav id="nav" role="navigation">
		<!-- <a href="#nav"  title="Show navigation">Show navigation</a>
	<a href="#" title="Hide navigation">Hide navigation</a> -->
		<a href="#" id="trigger" title="Show navigation">Show navigation</a>
		<ul class="clearfix">
			<li><a href='<spring:url value="/" />'>Home</a></li>
			
			<c:forEach var="category" items="${lstCategories}">
				<c:choose>
					<c:when test="${category.subCategories.size() != 0}">
						<li><a href='<spring:url value="/category/${category.alias}" />' aria-haspopup="true"><span>${category.name}</span></a>
						<ul>
							<c:forEach var="c" items="${category.subCategories}">
								<li><a href='<spring:url value="/category/${c.alias}" />'>${c.name}</a></li>
							</c:forEach>
							<%-- <li><a href='<spring:url value="/categories" />'><i class="fa fa-arrow-circle-down"></i> Show alls</a></li> --%>
						</ul>
					</li>
					</c:when>
					<c:otherwise>
						<li><a href='<spring:url value="/category/${category.alias}" />' aria-haspopup="true">${category.name}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</nav>
</div>
<!-- /header navigation -->