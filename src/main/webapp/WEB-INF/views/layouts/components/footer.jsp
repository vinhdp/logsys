<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container text-justify">
	<div class="row">
		<div class="col-md-3">
			<div class="page-header">
				<h4><a href='<spring:url value="/about" />'>About itechnotes.com</a></h4>
			</div>
			<p>itechnotes.com is a channel for sharing, learning experience in
				software development, and this is my favorite way to learn english.</p>
			<p>itechnotes.com is written using Java Spring MVC &
				Hibernate and still in development state!</p>
		</div>
		<div class="col-md-3">
			<div class="page-header">
				<h4>Find us?</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href='<spring:url value="/facebook" />'>Facebook</a></li>
				<li class="list-group-item"><a href='<spring:url value="/google-plus" />'>Google+</a></li>
				<li class="list-group-item"><a href='<spring:url value="/twiter" />'>Twiter</a></li>
				<li class="list-group-item"><a href='<spring:url value="/youtube" />'>Youtube</a></li>
			</ul>
		</div>
		<div class="col-md-3">
			<div class="page-header">
				<h4>Support!</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href='<spring:url value="/contact" />'>Contact us!</a></li>
				<li class="list-group-item"><a href='<spring:url value="/writing" />'>Writing?</a></li>
				<li class="list-group-item"><a href='<spring:url value="/reading" />'>Reading?</a></li>
				<li class="list-group-item"><a href='<spring:url value="/asking" />'>Asking?</a></li>
			</ul>
		</div>
		<div class="col-md-3">
			<div class="page-header">
				<h4>Privacy</h4>
			</div>
			<ul class="list-group">
				<li class="list-group-item"><a href='<spring:url value="/copyright" />'>Copyright</a></li>
				<li class="list-group-item"><a href='<spring:url value="/cooperate" />'>Cooperate</a></li>
				<%-- <li class="list-group-item"><a href='<spring:url value="/profit" />'>Donate</a></li> --%>
				<li class="list-group-item"><a href='<spring:url value="/development" />'>Development</a></li>
			</ul>
		</div>
	</div>
</div>
