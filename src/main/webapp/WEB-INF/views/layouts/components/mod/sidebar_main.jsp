<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
		<div class="pull-left image">
			<img
				src='<spring:url value="/resources/components/adminlte2/img/user2-160x160.jpg" />'
				class="img-circle" alt="User Image">
		</div>
		<div class="pull-left info">
			<p>VinhDP</p>
			<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
		</div>
	</div>
	<!-- search form -->
	<form action="#" method="get" class="sidebar-form">
		<div class="input-group">
			<input type="text" name="q" class="form-control"
				placeholder="Search..."> <span class="input-group-btn">
				<button type="submit" name="search" id="search-btn"
					class="btn btn-flat">
					<i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</form>
	<!-- /.search form -->
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu">
		<li class="header">MAIN NAVIGATION</li>
		<li class="treeview"><a href='<spring:url value="/itechs/posts" />'> <i class="fa fa-flag-checkered"></i>
				<span>All Posts</span></a></li>
		<li class="treeview"><a href='<spring:url value="/itechs/seos" />'> <i class="fa fa-usd"></i>
				<span>SEO</span></a></li>
	</ul>
</section>
<!-- /.sidebar -->
