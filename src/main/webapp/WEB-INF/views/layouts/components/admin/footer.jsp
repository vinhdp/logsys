<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="pull-right hidden-xs">
	<b>Version</b> 2.3.0
</div>
<strong>Copyright &copy; 2014-2015 <a
	href="http://almsaeedstudio.com">Almsaeed Studio</a>.
</strong>
All rights reserved.
