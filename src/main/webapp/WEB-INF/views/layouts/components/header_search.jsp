<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-sm-12">
			<a style="display:block;" class="text-center" title="Home" href='<spring:url value="/"></spring:url>'>
				<img id="site-logo" class="img-responsive" alt="Logsys logo" src='<spring:url value="/resources/themes/img/logo.png" />'>
			</a>
		</div>
		<div class="col-md-4 col-sm-12">
			<form>
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
  							<script>
							  (function() {
							    var cx = '014441952256662679076:mrqldyxxovg';
							    var gcse = document.createElement('script');
							    gcse.type = 'text/javascript';
							    gcse.async = true;
							    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
							    var s = document.getElementsByTagName('script')[0];
							    s.parentNode.insertBefore(gcse, s);
							  })();
							</script>
							<gcse:searchbox-only resultsUrl='<spring:url value="/search" />'></gcse:searchbox-only>
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<!-- /.row -->
			</form>
		</div>
		<div class="col-md-4 col-sm-12">
			
		</div>
	</div>
</div>