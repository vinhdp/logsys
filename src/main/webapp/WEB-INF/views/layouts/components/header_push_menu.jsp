<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<tiles:importAttribute name="lstCategories" />
<!-- mp-menu -->
<nav id="mp-menu" class="mp-menu">
	<div class="mp-level">
		<h2 class="icon icon-world">All Categories</h2>
		<ul>
			<c:forEach var="category" items="${lstCategories}">
				<c:choose>
					<c:when test="${category.subCategories.size() != 0}">
						<li class="icon icon-arrow-left"><a class="icon icon-display" href='<spring:url value="/category/${category.alias}" />'>${category.name}</a>
						<div class="mp-level">
							<h2 class="icon icon-display">${category.name}</h2>
							<a class="mp-back" href="#">back</a>
							<ul>
								<c:forEach var="c" items="${category.subCategories}">
									<li><a class="icon icon-phone" href='<spring:url value="/category/${c.id}" />'>${c.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</c:when>
					<c:otherwise>
						<li><a class="icon icon-photo" href='<spring:url value="/category/${category.alias}" />'>${category.name}</a></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>
</nav>
<!-- /mp-menu -->