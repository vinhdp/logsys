<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tiles:importAttribute name="lstNewPosts"/>
<tiles:importAttribute name="lstRandomPosts"/>
<div class="page-header">
	<h2>New Post</h2>
</div>
<c:forEach var="post" items="${lstNewPosts}">
	<div class="col-md-4 col-sm-6 col-xs-12 post-item">
		<div class="col-md-4 col-sm-3 col-xs-4">
			<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'> <img class="img-responsive"
				src='${post.image}' alt="${post.title}"></a>
		</div>
		<div class="col-md-8 col-sm-9 col-xs-8">
			<h4 class="media-heading">
				<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'>${post.title}</a>
			</h4>
			<p class="entry-meta">
				<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
				<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
			</p>
		</div>
	</div>
</c:forEach>
<div class="clearfix"></div>
<div class="page-header">
	<h2>Other Post</h2>
</div>
<c:forEach var="post" items="${lstRandomPosts}">
	<div class="col-md-4 col-sm-6 col-xs-12 post-item">
		<div class="col-md-4 col-sm-3 col-xs-4">
			<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'> <img class="img-responsive"
				src='${post.image}' alt="${post.title}"></a>
		</div>
		<div class="col-md-8 col-sm-9 col-xs-8">
			<h4 class="media-heading">
				<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'>${post.title}</a>
			</h4>
			<p class="entry-meta">
				<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
				<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
			</p>
		</div>
	</div>
</c:forEach>