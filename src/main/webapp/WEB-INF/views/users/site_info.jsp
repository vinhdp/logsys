<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item no-padding">
				<div class="page-header">
					<h1>
						<strong>${post.title}</strong> 
					</h1>
				</div>
				<div class="page-info">
					<div class="pull-left">
						<p class="entry-meta">
							<i class="fa fa-user"></i> itechnotes.com
						</p>
					</div>
					<div class="pull-right clearfix-sm">
						<div class="fb-like" data-href='<spring:url value="/${post.alias}"></spring:url>' data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
						<!-- <a class="btn btn-xs btn-social-icon btn-facebook" href="/share-facebook">
							<span class="fa fa-facebook"></span>
						</a> -->
						<!-- <a class="btn btn-xs btn-social-icon btn-google" href="/share-google">
							<span class="fa fa-google-plus"></span>
						</a>
						<a class="btn btn-xs btn-social-icon btn-twitter" href="/share-twitter">
							<span class="fa fa-twitter"></span>
						</a> -->
					</div>
					<div class="clearfix"></div>
				</div>
				${post.content }
			</li>
		</ul>
	</div>
</div>