<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item">
				<div class="page-header">
					<h1>Members</h1>
				</div>
				<c:forEach var="member" items="${lstMembers}">
					<div class="row">
						<div class="col-md-3 hidden-sm hidden-xs">
							<a href="#"> <img class="img-responsive img-profile"
								src='<spring:url value="/resources/themes/img/default-profile.jpg" />' alt="Picture description"></a>
						</div>
						<div class="col-md-9 col-sm-12 col-xs-12">
							<h4 class="media-heading">
								<a href="#">${member.firstName} ${member.lastName}</a>
							</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
							<p><b>Joined:</b> <fmt:formatDate type="date" value="${member.createDate}" /></p>
							<p><b>Company:</b> VNG</p>
						</div>
					</div>
				</c:forEach>
				<div class="clearfix"></div>
				<div class="center">
					<div id="page-selection" data-total-pages="${totalPages}"
						data-page-number="${pageNumber}"></div>
				</div>
			</li>
		</ul>
	</div>
</div>