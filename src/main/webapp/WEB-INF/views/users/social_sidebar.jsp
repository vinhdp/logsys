<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-4 col-sm-5">
	<div class="sidebar">
		<div class="panel panel-primary">
			<!-- <div class="panel-heading">Advertise</div> -->
			<!-- <div class="panel-body">Panel content</div> -->
			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="page-header text-center">
						<h2>Facebook</h2>
					</div>
					<div class="row">
					<div class="col-md-12 text-center">
						<div class="fb-page" data-href="https://www.facebook.com/itechnotes/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/itechnotes/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/itechnotes/">ITechNotes.com</a></blockquote></div>
					</div>
					</div>
				</li>
				<li class="list-group-item"><a href="#"> <!-- <img class="img-responsive" src="resources/themes/img/ad1.jpg" alt="Picture description"> -->
				</a></li>
				<li class="list-group-item"><a href="#"> <!-- <img class="img-responsive" src="resources/themes/img/ad2.jpg" alt="Picture description"> -->
				</a></li>
			</ul>
		</div>
		<div class="panel panel-primary">
			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="page-header text-center">
						<h2>Search</h2>
					</div>
				</li>
				<li class="list-group-item no-padding">
					<form action='<spring:url value="/search"></spring:url>'>
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<input type="text" name="q" class="form-control input-search"
										placeholder="Find a blog post!" value="${q}"> <span
										class="input-group-btn">
										<button class="btn btn-primary btn-flat" type="submit">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
								<!-- /input-group -->
							</div>
						</div>
						<!-- /.row -->
					</form>
				</li>
			</ul>
		</div>
		<div class="panel panel-primary">
			<!-- <div class="panel-heading">Advertise</div> -->
			<!-- <div class="panel-body">Panel content</div> -->
			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="page-header text-center">
						<h2>Advertise</h2>
					</div>
				</li>
				<li class="list-group-item">
					<a href="#">
					</a>
				</li>
			</ul>
		</div>
		<div class="panel panel-primary">
			<!-- <div class="panel-heading">Search Box</div> -->
			<!-- <div class="panel-body">Panel content</div> -->
			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="alert alert-info text-center panel-heading"
						role="alert">Follow us!</div>
					<div class="text-center">
						<a class="btn btn-block btn-social btn-facebook">
    						<span class="fa fa-facebook"></span> Find us on facebook!
  						</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>