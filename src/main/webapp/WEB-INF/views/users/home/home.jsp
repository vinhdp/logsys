<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item no-padding">
				<div class="page-header">
					<h2>New Post</h2>
				</div>
				<div class="row" id="new-post">
					<c:forEach var="post" items="${lstNewPosts}">
						<div class="col-md-6 col-sm-12 col-xs-12 post-item">
							<div class="col-md-4 col-sm-12 col-xs-4">
								<a href='<spring:url value="/post/${post.alias}" />'> <img class="img-responsive"
									src='${post.image}' alt="${post.title}"></a>
							</div>
							<div class="col-md-8 col-sm-12 col-xs-8">
								<h4 class="media-heading">
									<a href='<spring:url value="/post/${post.alias}" />'>${post.title}</a>
								</h4>
								<p class="entry-meta">
									<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
									<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
								</p>
							</div>
						</div>
					</c:forEach>
				</div>
				<div class="clearfix"></div>
				<div class="pull-right">
					<h4><a href="posts" class="btn btn-primary btn-flat btn-no-border">Show more</a></h4>
				</div>
				<div class="clearfix"></div>
			</li>
			<li class="list-group-item no-padding">
				<div class="page-header">
					<h2>Our Category</h2>
				</div>
				<c:forEach var="category" items="${lstCategories}">
					<div class="row post-item">
						<div class="col-md-3 col-sm-4 col-xs-4">
							<a href='<spring:url value="/category/${category.alias}" />'> <img
								class="img-responsive img-profile" 
								src='${category.image}' alt="${category.title}">
							</a>
						</div>
						<div class="col-md-9 col-sm-8 col-xs-8">
							<h3 class="media-heading">
								<a href='<spring:url value="/category/${category.alias}" />'>${category.name}</a>
							</h3>
							<p>${category.description}</p>
							<p class="entry-meta">
								<!-- <i class="fa fa-clock-o"></i> <time class="timeago" datetime="2016-06-26 00:11:22">2016-06-26</time> -->
							</p>
						</div>
					</div>
				</c:forEach>
				<div class="clearfix"></div>
				<div class="pull-right">
					<h4><a href="categories" class="btn btn-primary btn-flat btn-no-border">Show more</a></h4>
				</div>
				<div class="clearfix"></div>
			</li>
		</ul>
	</div>
</div>