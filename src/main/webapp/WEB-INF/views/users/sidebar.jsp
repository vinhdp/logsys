<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-4 col-sm-5">
	<div class="sidebar">
		<c:if test="${lstRelatedPosts != null && lstRelatedPosts.size() != 0}">
			<div class="panel panel-primary">
				<!-- <div class="panel-heading">Related Posts</div> -->
				<!-- <div class="panel-body">Panel content</div> -->
				<!-- List group -->
				<ul class="list-group">
					<li class="list-group-item no-padding">
						<div class="page-header text-center">
							<h2>Related Posts</h2>
						</div>
					</li>
					<c:forEach var="post" items="${lstRelatedPosts}">
						<li class="list-group-item post-item no-padding">
							<div class="row">
								<div class="col-md-4 hidden-sm col-xs-4">
									<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'> <img class="img-responsive"
										src='${post.image}' alt="${post.title}"></a>
								</div>
								<div class="col-md-8 col-sm-12 col-xs-8">
									<h4 class="media-heading">
										<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'>${post.title}</a>
									</h4>
									<p class="entry-meta">
										<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
										<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
									</p>
								</div>
							</div>
						</li>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<div class="panel panel-primary">
			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="page-header text-center">
						<h2>Search</h2>
					</div>
				</li>
				<li class="list-group-item no-padding">
					<form action='<spring:url value="/search"></spring:url>'>
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<input type="text" name="q" class="form-control input-search"
										placeholder="Find a blog post!" value="${q}"> <span
										class="input-group-btn">
										<button class="btn btn-primary btn-flat" type="submit">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
								<!-- /input-group -->
							</div>
						</div>
						<!-- /.row -->
					</form>
				</li>
			</ul>
		</div>
		<!-- <div class="panel panel-primary">
			<div class="panel-heading">Advertise</div>
			<div class="panel-body">Panel content</div>
			List group
			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="page-header text-center">
						<h1>Advertise</h1>
					</div>
				</li>
				<li class="list-group-item"><a href="#"> <img class="img-responsive" src="resources/themes/img/ad1.jpg" alt="Picture description">
				</a></li>
				<li class="list-group-item"><a href="#"> <img class="img-responsive" src="resources/themes/img/ad2.jpg" alt="Picture description">
				</a></li>
			</ul>
		</div> -->
		<c:if test="${lstTopPosts.size() != null && lstRelatedPosts.size() != 0}">
			<div class="panel panel-primary">
				<!-- <div class="panel-heading">Top Posts</div> -->
				<!-- <div class="panel-body">Panel content</div> -->
				<!-- List group -->
				<ul class="list-group">
					<li class="list-group-item no-padding">
						<div class="page-header text-center">
							<h2>Top Posts</h2>
						</div>
					</li>
					<c:forEach var="post" items="${lstTopPosts}">
						<li class="list-group-item post-item no-padding">
							<div class="row">
								<div class="col-md-4 hidden-sm col-xs-4">
									<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'> <img class="img-responsive"
										src='${post.image}' alt="${post.title}"></a>
								</div>
								<div class="col-md-8 col-sm-12 col-xs-8">
									<h4 class="media-heading">
										<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'>${post.title}</a>
									</h4>
									<p class="entry-meta">
										<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
										<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
									</p>
								</div>
							</div>
						</li>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<div class="panel panel-primary">
			<!-- List group -->
			<ul class="list-group">
				<li class="list-group-item no-padding" style="padding-top: 10px;">
					<div class="alert alert-info text-center panel-heading"
						role="alert">Find us?</div>
					<div class="text-center">
						<a href="https://www.facebook.com/itechnotes/" rel="nofollow" target="_blank" class="btn btn-block btn-social btn-facebook">
    						<span class="fa fa-facebook"></span> Find us on facebook!
  						</a>
						<!-- <a class="btn btn-social-icon btn-google"><span
							class="fa fa-google-plus"></span></a> <a
							class="btn btn-social-icon btn-twitter"><span
							class="fa fa-twitter"></span></a> <a
							class="btn btn-social-icon btn-github"><span
							class="fa fa-github"></span></a> -->
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>