<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item no-padding">
				<div class="page-header">
					<h1>Our posts</h1>
				</div>
				<c:forEach var="post" items="${lstPosts}">
					<div class="row post-item">
						<div class="col-md-3 col-sm-4 col-xs-4">
							<a href='<spring:url value="/post/${post.alias}" />'> <img class="img-responsive img-profile"
								src='${post.image}' alt="${post.title}"></a>
						</div>
						<div class="col-md-9 col-sm-8 col-xs-8">
							<h4 class="media-heading">
								<a href='<spring:url value="/post/${post.alias}" />'>${post.title}</a>
							</h4>
							<p class="block-with-text">${post.description}</p>
							<p class="entry-meta">
								<i class="fa fa-user"></i> ${post.author.firstName} ${post.author.lastName}&thinsp;&thinsp;
								<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
							</p>
						</div>
					</div>
				</c:forEach>
				<div class="clearfix"></div>
				<div class="center">
					<div id="page-selection" data-total-pages="${totalPages}"
						data-page-number="${pageNumber}"></div>
				</div>
			</li>
		</ul>
	</div>
</div>