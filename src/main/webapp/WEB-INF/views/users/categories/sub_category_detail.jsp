<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item no-padding">
				<c:choose>
					<c:when test="${post != null}">
						<div class="page-header">
							<h1>${post.title}</h1>
						</div>
						<div class="page-info">
							<div class="pull-left">
								<p class="entry-meta">
									<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
									<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
								</p>
							</div>
							<div class="pull-right clearfix-sm">
								<div class="fb-like" data-href='<spring:url value="/post/${post.alias}"></spring:url>' data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div>${post.content}</div>
						<div class="fb-comments" data-href='http://itechnotes.com/post/${post.alias}' data-numposts="5"></div>
						<div class="page-header">
							<h2>${category.name}</h2>
						</div>
					</c:when>
					<c:otherwise>
						<div class="page-header">
							<h1>${category.name}</h1>
						</div>
					</c:otherwise>
				</c:choose>
				<c:forEach var="post" items="${lstPosts}">
					<div class="row post-item">
						<div class="col-md-3 hidden-sm col-xs-4">
							<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'> <img class="img-responsive img-profile"
								src='${post.image}' alt="${post.title}"></a>
						</div>
						<div class="col-md-9 col-sm-12 col-xs-8">
							<h3 class="media-heading">
								<a href='<spring:url value="/${post.category.alias}/${post.alias}" />'>${post.title}</a>
							</h3>
							<p class="entry-meta">
								<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
								<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
							</p>
						</div>
					</div>
				</c:forEach>
				<div class="clearfix"></div>
				<c:choose>
					<c:when test="${totalElements > pageSize}">
						<div class="center">
							<div id="page-selection" data-total-pages="${totalPages}"
								data-page-number="${pageNumber}"></div>
						</div>
					</c:when>
					<c:when test="${totalElements == 0}">
						<div class="alert alert-info" role="alert">This category has no post!</div>
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
			</li>
		</ul>
	</div>
</div>