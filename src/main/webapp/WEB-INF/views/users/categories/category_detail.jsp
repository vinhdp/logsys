<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item no-padding">
				<c:choose>
					<c:when test="${post != null}">
						<div class="page-header">
							<h2>${post.title}</h2>
						</div>
						<div class="page-info">
							<div class="pull-left">
								<p class="entry-meta">
									<i class="fa fa-user"></i> ${post.author.firstName}&thinsp;&thinsp;
									<i class="fa fa-clock-o"></i> <time class="timeago" datetime="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${post.createdDate}" />"><fmt:formatDate type="date" value="${post.createdDate}" /></time>
								</p>
							</div>
							<div class="pull-right clearfix-sm">
								<div class="fb-like" data-href='<spring:url value="/post/${post.alias}"></spring:url>' data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div>${post.content}</div>
						<div class="fb-comments" data-href='http://itechnotes.com/post/${post.alias}' data-numposts="5"></div>
					<div class="page-header">
							<h2>${category.name}</h2>
						</div>
					</c:when>
					<c:otherwise>
						<div class="page-header">
							<h1>${category.name}</h1>
						</div>
					</c:otherwise>
				</c:choose>
				<!-- Nav tabs -->
				<div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                    	<li role="presentation" class="active"><a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">Posts</a></li>
                        <li role="presentation"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
                    </ul>

                       <!-- Tab panes -->
                       <div class="tab-content">
                       	<div role="tabpanel" class="tab-pane active" id="posts" name="posts">
							<div id="appPost" ng-app="posts">
                           		<div ng-controller="PostController">
								    <div class="row post-item" ng-repeat="post in lstPosts">
										<div class="col-md-3 col-sm-4 col-xs-4">
											<a href='<spring:url value="/{{post.category.alias}}/{{post.alias}}" />'> <img class="img-responsive img-profile"
												src='{{post.image}}' alt="{{post.title}}"></a>
										</div>
										<div class="col-md-9 col-sm-8 col-xs-8">
											<h3 class="media-heading">
												<a href='<spring:url value="/{{post.category.alias}}/{{post.alias}}"/>' ng-bind="post.title"></a>
											</h3>
											<!-- <p ng-bind="post.description" class="block-with-text"></p> -->
											<p class="entry-meta">
												<i class="fa fa-user"></i> <span ng-bind="post.author.firstName">${post.author.firstName}</span>&thinsp;&thinsp;
												<i class="fa fa-clock-o"></i> <time class="timeago" datetime="{{post.createdDate}}" ng-bind="post.createdDate | timeAgo"></time>
											</p>
										</div>
									</div>
									<div class="alert alert-info" role="alert" ng-hide="lstPosts.length">This category has no post!</div>
									<div class="clearfix"></div>
									<div class="center">
									    <pagination ng-hide="visible" class="ng-hide"
									      ng-model="currentPage"
									      total-items="totalItems"
									      items-per-page="numPerPage"
									      max-size="maxSize"
									      force-ellipses="true"
									      ng-disabled="false"
									      boundary-links="true" previous-text="&laquo;" next-text="&raquo;" first-text="←" last-text="→">
									    </pagination>
								    </div>
                           		</div>
							</div>
						</div>
                           <div role="tabpanel" class="tab-pane" id="categories" data-id="${category.id}" name="categories">
                           	<div id="appCategory" ng-app="categories">
                           		<div ng-controller="CategoryController">
                            		<div class="row post-item" ng-repeat="category in lstCategories">
									    <div class="col-md-3 col-sm-4 col-xs-4">
											<a href='<spring:url value="/category/{{category.alias}}" />'> <img
												class="img-responsive" 
												src='{{category.image}}' alt="{{category.title}}">
											</a>
										</div>
										<div class="col-md-9 col-sm-8 col-xs-8">
											<h3 class="media-heading">
												<a href='<spring:url value="/category/{{category.alias}}" />'>{{category.name}}</a>
											</h3>
											<p class="block-with-text">{{category.description}}</p>
										</div>
										<div class="alert alert-info" role="alert" ng-hide="lstCategories.length">This category has no sub category!</div>
                            		</div>
                            		<div class="clearfix"></div>
										<div class="center">
										    <pagination ng-hide="visible" class="ng-hide"
										      ng-model="currentPage"
										      total-items="totalItems"
										      items-per-page="numPerPage"
										      max-size="maxSize"
										      force-ellipses="true"
										      boundary-links="true" previous-text="&laquo;" next-text="&raquo;" first-text="←" last-text="→">
										    </pagination>
									    </div>
                           		</div>
							</div>
                           </div>
                       </div>
				</div>
			</li>
		</ul>
	</div>
</div>