<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item no-padding">
				<div class="page-header">
					<h1>Our Categories</h1>
				</div>
				
				<c:forEach var="category" items="${lstCategories}">
					<div class="row post-item">
						<div class="col-md-3 col-sm-4 col-xs-4">
							<a href='<spring:url value="/category/${category.alias}" />'> <img
								class="img-responsive img-profile"
								src='${category.image}' alt="${category.title}">
							</a>
						</div>
						<div class="col-md-9 col-sm-8 col-xs-8">
							<h3 class="media-heading">
								<a href='<spring:url value="/category/${category.alias}" />'>${category.name}</a>
							</h3>
							<p>${category.description}</p>
							<p class="entry-meta">
								<!-- <i class="fa fa-clock-o"></i> <time class="timeago" datetime="2016-06-26 00:11:22">2016-06-26</time> -->
							</p>
						</div>
					</div>
				</c:forEach>
				<div class="clearfix"></div>
				<div class="center">
					<div id="page-selection" data-total-pages="${totalPages}"
						data-page-number="${pageNumber}"></div>
				</div>
			</li>
		</ul>
	</div>
</div>