<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- row -->
<div class="row" id="postPage">
	<div class="col-xs-12">
		<div class="box">
			<form:form action="../${postDTO.categoryId}/add" modelAttribute="postDTO" method="post">
				<div class="box-header with-border">
					<h2 class="box-title">${postDTO.title}</h2>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="mailbox-controls" id="controlMenu">
						<!-- Check all button -->
						<span id="controlGroup">
							<a href='<spring:url value="/itechs/post/${postDTO.id}/update" />' class="btn btn-default btn-sm" title="Update post"><i class="fa fa-pencil-square-o"></i></a>
							<a id="delete" class="btn btn-default btn-sm"  title="Delete this category!"><i class="fa fa-trash-o"></i></a>
						</span>
						<span id="confirmDeletion" class="hidden">
				            <span class="text-red"><i class="icon fa fa-exclamation-triangle"></i> Deletion Warning! Do you want to delete this post?
							</span>
							<a href='<spring:url value="/itechs/post/${postDTO.id}/delete" />' class="" title="Delete immediately!"><i class="fa fa-check"></i></a>
							<a href='#' id="discardDeletion" title="Discard action!"><i class="fa fa-times"></i></a>
						</span>
						<span id="controlGroup" class="pull-right">
							<a href='<spring:url value="/itechs/seo/update?id=${seoId}&typeId=${postDTO.id}" />' class="btn btn-default btn-sm" title="Edit SEO Information"><i class="fa fa-globe"></i></a>
						</span>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Title</label> <form:errors path="title" class="form-error" />
								<form:input path="title" type="text" placeholder="Title" id="title" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Alias</label> <form:errors path="alias" class="form-error" />
								<!-- <input type="text" class="form-control" id="" placeholder="Alias" value=""> -->
								<form:input path="alias" type="text" placeholder="Alias" id="alias" class="form-control" />
							</div>
							<div class="form-group">
								<label>Super Category</label> <form:errors path="categoryId" class="form-error" />
								<form:select id="category" path="categoryId" data-category-id="${postDTO.categoryId}"
									class="form-control select2 select2-hidden-accessible"
									style="width: 100%;" tabindex="-1" aria-hidden="true">
									<form:option value="0">NONE</form:option>
									<form:options items="${lstCategories}" itemValue="id" itemLabel="name"></form:options>
								</form:select>
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Create Date</label> <form:errors path="createdDate" class="form-error" />
								<!-- <input type="text" class="form-control" id="" placeholder="Alias" value=""> -->
								<form:input path="createdDate" type="text" placeholder="Create Date" id="createdDate" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Publish Date</label> <form:errors path="publishedDate" class="form-error" />
								<!-- <input type="text" class="form-control" id="" placeholder="Alias" value=""> -->
								<form:input path="publishedDate" type="text" placeholder="Publish Date" id="publishedDate" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Last Modify Date</label> <form:errors path="modifiedDate" class="form-error" />
								<!-- <input type="text" class="form-control" id="" placeholder="Alias" value=""> -->
								<form:input path="modifiedDate" type="text" placeholder="Last Modify Date" id="modifiedDate" class="form-control" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="exampleInputEmail1">View</label> <form:errors path="view" class="form-error" />
								<form:input path="view" type="text" placeholder="View" id="view" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Like</label> <form:errors path="like" class="form-error" />
								<form:input path="like" type="text" placeholder="Parent Order" id="like" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Share</label> <form:errors path="share" class="form-error" />
								<form:input path="share" type="text" placeholder="Parent Order" id="share" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Status</label>
								<form:select path="status"  data-status-value="${postDTO.status}"
									class="form-control select2 select2-hidden-accessible"
									style="width: 100%;" tabindex="-1" aria-hidden="true">
									<form:option value="NEW" label="NEW"></form:option>
									<form:option value="PUBLISHED" label="PUBLISHED"></form:option>
									<form:option value="EDITTING" label="EDITTING"></form:option>
									<form:option value="REMOVED" label="REMOVED"></form:option>
								</form:select>
							</div>
							<div class="form-group">
								<label></label>
								<a href="${postDTO.id}/update" class="btn btn-primary">Update</a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Keyword</label> <form:errors path="keyword" class="form-error" />
						<form:textarea path="keyword" rows="3" placeholder="Keyword" id="keyword" class="form-control" />
					</div>
					<div class="form-group">
						<label>Description</label> <form:errors path="description" class="form-error" />
						<form:textarea path="description" rows="4" placeholder="Description" id="description" class="form-control" />
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Image Link</label>
						<form:input path="image" type="text" placeholder="Image Link" id="image" class="form-control" />
					</div>
					<div class="form-group">
						<label>Content</label>
						<br>
						${postDTO.content}
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer with-border">
					<a href="${postDTO.id}/update" class="btn btn-primary">Update</a>
				</div>
			</form:form>
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->