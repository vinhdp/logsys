<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- row -->
<div class="row" id="postPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">All Posts</h3>
			</div>
			<!-- /.box-header -->
			<div ng-app="posts" ng-controller="PostController">
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<div class="row">
							<div class="col-md-1 p-b-5">
								<a href='<spring:url value="/inotes/post/{{category}}/add" />' class="btn btn-default btn-sm" title="Add new post"><i class="fa fa-plus-square"></i></a>
							</div>
							<div class="col-md-4 p-b-5">
								<select ng-model="category" id="category" class="form-control select2 select2-hidden-accessible"
									tabindex="-1" aria-hidden="true"  style="width: 100%">
									<option value="0">ALL</option>
									<c:forEach var="category" items="${lstCategories}">
										<option value="${category.id}">${category.name}</option>
									</c:forEach>
								</select>
							</div>
							<div class="box-tools pull-right col-xs-12 col-sm-12 col-md-6">
			                	<div class="has-feedback">
			                    	<input id="searchBox" ng-model="searchTitle" ng-change="textChanged()" type="text" class="form-control" placeholder="Search Post">
			                    	<span class="glyphicon glyphicon-search form-control-feedback"></span>
			                    </div>
			           		</div>
		               </div>
					</span>
				</div>
			</div>
			<div class="box-body">
				<div id="example2_wrapper"
					class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="allPosts" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Title</th>
										<th>Alias</th>
										<th>Status</th>
										<th>Views</th>
										<th>Likes</th>
										<th>Shares</th>
										<th class="hidden-xs hidden-sm">Image</th>
										<th class="hidden-xs hidden-sm">Created Date</th>
										<!-- <th class="hidden-xs hidden-sm">Published Date</th> -->
										<th class="hidden-xs hidden-sm">Modified Date</th>
									</tr>
								</thead>
								<tbody id="tableBody">
									<%-- <c:forEach var="post" items="${lstPosts}">
										<tr>
											<td><a href='<spring:url value="/inotes/post/${post.id}" />'>${post.title}</a></td>
											<td>${post.alias}</td>
											<td>${post.status}</td>
											<td>${post.view}</td>
											<td>${post.like}</td>
											<td>${post.share}</td>
											<td class="hidden-xs hidden-sm">${post.image}</td>
											<td class="hidden-xs hidden-sm">${post.createdDate}</td>
											<td class="hidden-xs hidden-sm">${post.publishedDate}</td>
											<td class="hidden-xs hidden-sm">${post.modifiedDate}</td>
										</tr>
									</c:forEach> --%>
									<tr>
										<th>Title</th>
										<th>Alias</th>
										<th>Status</th>
										<th>Views</th>
										<th>Likes</th>
										<th>Shares</th>
										<th class="hidden-xs hidden-sm">Image</th>
										<th class="hidden-xs hidden-sm">Created Date</th>
										<!-- <th class="hidden-xs hidden-sm">Published Date</th> -->
										<th class="hidden-xs hidden-sm">Modified Date</th>
									</tr>
									<tr ng-repeat="post in lstPosts" after-render="formatDateTime()">
											<td><a href='<spring:url value="/inotes/post/{{post.id}}" />'>{{post.title}}</a></td>
											<td>{{post.alias}}</td>
											<td>{{post.status}}</td>
											<td>{{post.view}}</td>
											<td>{{post.like}}</td>
											<td>{{post.share}}</td>
											<td>{{post.image}}</td>
											<td class="datetime hidden-xs hidden-sm">{{post.createdDate}}</td>
											<!-- <td class="datetime hidden-xs hidden-sm">{{post.publishedDate}}</td> -->
											<td class="datetime hidden-xs hidden-sm">{{post.modifiedDate}}</td>
									</tr>
								</tbody>
							</table>
							<div class="clearfix"></div>
							<c:choose>
								<c:when test="${totalElements > pageSize}">
									<div class="center">
										<div id="page-selection" data-total-pages="${totalPages}"
											data-page-number="${pageNumber}"></div>
									</div>
								</c:when>
								<c:when test="${totalElements == 0}">
									<div class="alert alert-info" role="alert">There is no post!</div>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
							<div class="center">
							    <pagination ng-hide="visible" class="ng-hide"
							      ng-model="currentPage"
							      total-items="totalItems"
							      items-per-page="numPerPage"
							      max-size="maxSize"
							      force-ellipses="true"
							      ng-disabled="false"
							      boundary-links="true" previous-text="&laquo;" next-text="&raquo;" first-text="←" last-text="→">
							    </pagination>
						    </div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			</div>
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->