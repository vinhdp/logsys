<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- row -->
<div class="row" id="categoryPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title">${categoryDTO.name}</h2>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool"
						data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="mailbox-controls control-menu" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a href='<spring:url value="/inotes/category/${categoryDTO.id}/add" />' class="btn btn-default btn-sm" title="Add new sub category!"><i class="fa fa-plus-square"></i></a>
						<a href='<spring:url value="/inotes/category/${categoryDTO.id}/update" />' class="btn btn-default btn-sm" title="Update category!"><i class="fa fa-pencil-square-o"></i></a>
						<a id="delete" class="btn btn-default btn-sm"  title="Delete this category!"><i class="fa fa-trash-o"></i></a>
					</span>
					<span id="confirmDeletion" class="hidden">
			            <span class="text-red"><i class="icon fa fa-exclamation-triangle"></i> Deletion Warning! Do you want to delete this category?
						</span>
						<a href='<spring:url value="/inotes/category/${categoryDTO.id}/delete" />' class="" title="Delete immediately!"><i class="fa fa-check"></i></a>
						
						<a href='#' id="discardDeletion" title="Discard action!"><i class="fa fa-times"></i></a>
					</span>
					<span id="controlGroup" class="pull-right">
						<a href='<spring:url value="/inotes/post/${categoryDTO.id}/add" />' class="btn btn-default btn-sm" title="Create new post"><i class="fa fa-plus-square"></i></a>
						<a href='<spring:url value="/inotes/category/${categoryDTO.id}/update" />' class="btn btn-default btn-sm" title="Show all post"><i class="fa fa-list-alt"></i></a>
						<a href='<spring:url value="/inotes/seo/update?id=${seoId}&typeId=${categoryDTO.id}" />' class="btn btn-default btn-sm" title="Edit SEO Information"><i class="fa fa-globe"></i></a>
					</span>
				</div>
			</div>
			<!-- /.box-header -->
			<form:form action="${categoryDTO.id}/update" modelAttribute="categoryDTO" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Name</label> <form:errors path="name" class="form-error" />
								<form:input path="name" type="text" placeholder="Name" id="name" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Title</label> <form:errors path="title" class="form-error" />
								<!-- <input type="text" class="form-control" id="" placeholder="Title" value=""> -->
								<form:input path="title" type="text" placeholder="Title" id="title" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Alias</label> <form:errors path="alias" class="form-error" />
								<!-- <input type="text" class="form-control" id="" placeholder="Alias" value=""> -->
								<form:input path="alias" type="text" placeholder="Alias" id="alias" class="form-control" />
							</div>
							<div class="form-group">
								<label>Super Category</label>
								<form:select id="superCategory" path="superCategoryId" data-super-id="${categoryDTO.superCategoryId}"
									class="form-control select2 select2-hidden-accessible"
									style="width: 100%;" tabindex="-1" aria-hidden="true">
									<form:option value="0">NONE</form:option>
									<form:options items="${lstCategories}" itemValue="id" itemLabel="name"></form:options>
								</form:select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Menu Order</label> <form:errors path="menuOrder" class="form-error" />
								<form:input path="menuOrder" type="text" placeholder="Menu Order" id="menuOrder" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Parent Order</label> <form:errors path="parentOrder" class="form-error" />
								<form:input path="parentOrder" type="text" placeholder="Parent Order" id="parentOrder" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Status</label>
								<form:select path="status"  data-status-value="${categoryDTO.status}"
									class="form-control select2 select2-hidden-accessible"
									style="width: 100%;" tabindex="-1" aria-hidden="true">
									<form:option value="SHOW" label="SHOW"></form:option>
									<form:option value="HIDDEN" label="HIDDEN"></form:option>
								</form:select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Keyword</label> <form:errors path="keyword" class="form-error" />
						<form:textarea path="keyword" rows="3" placeholder="Keyword" id="keyword" class="form-control" />
					</div>
					<div class="form-group">
						<label>Description</label> <form:errors path="description" class="form-error" />
						<form:textarea path="description" rows="4" placeholder="Description" id="description" class="form-control" />
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Image Link</label>
						<form:input path="image" type="text" placeholder="Image Link" id="image" class="form-control" />
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer with-border">
					<a href="${categoryDTO.id}/update" class="btn btn-primary">Update</a>
				</div>
			</form:form>
		</div>
		<!-- /.box -->

		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title">${categoryDTO.name} sub categories</h2>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool"
						data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="allCategories" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Title</th>
							<th>Alias</th>
							<th>Keyword</th>
							<th>Description</th>
							<th>Menu Order</th>
							<th>Parent Order</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="category" items="${lstSubCategories}">
							<tr>
								<td><a
									href='<spring:url value="/inotes/category/${category.id}" />'>${category.name}</a></td>
								<td>${category.title}</td>
								<td>${category.alias}</td>
								<td>${category.keyword}</td>
								<td>${category.description}</td>
								<td>${category.menuOrder}</td>
								<td>${category.parentOrder}</td>
								<td>${category.status}</td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Title</th>
							<th>Alias</th>
							<th>Keyword</th>
							<th>Description</th>
							<th>Menu Order</th>
							<th>Parent Order</th>
							<th>Status</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->