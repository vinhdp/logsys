<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- row -->
<div class="row" id="categoryPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">All Categories</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a href='<spring:url value="/inotes/category/0/add" />' class="btn btn-default btn-sm" title="Add new category!"><i class="fa fa-plus-square"></i></a>
					</span>
				</div>
			</div>
			<div class="box-body">
				<div id="example2_wrapper"
					class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="allCategories" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Name</th>
										<th>Title</th>
										<th>Alias</th>
										<th>Keyword</th>
										<th>Description</th>
										<th>Menu Order</th>
										<th>Parent Order</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="category" items="${lstCategories}">
										<tr>
											<td><a href='<spring:url value="/inotes/category/${category.id}" />'>${category.name}</a></td>
											<td>${category.title}</td>
											<td>${category.alias}</td>
											<td>${category.keyword}</td>
											<td>${category.description}</td>
											<td>${category.menuOrder}</td>
											<td>${category.parentOrder}</td>
											<td>${category.status}</td>
										</tr>
									</c:forEach>
								</tbody>
								<tfoot>
									<tr>
										<th>Name</th>
										<th>Title</th>
										<th>Alias</th>
										<th>Keyword</th>
										<th>Description</th>
										<th>Menu Order</th>
										<th>Parent Order</th>
										<th>Status</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->