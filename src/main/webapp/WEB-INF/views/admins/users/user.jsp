<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- row -->
<div class="row" id="userPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">All Users</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a href='<spring:url value="/inotes/user/add" />' class="btn btn-default btn-sm" title="Add new user"><i class="fa fa-plus-square"></i></a>
					</span>
				</div>
			</div>
			<div class="box-body">
				<div id="example2_wrapper"
					class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6"></div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table id="allUsers" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Birthday</th>
										<th>Sex</th>
										<th>Created Date</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="user" items="${lstUsers}">
										<tr>
											<td><a href='<spring:url value="/inotes/user/${user.id}" />'>${user.firstName}</a></td>
											<td>${user.lastName}</td>
											<td>${user.email}</td>
											<td>${user.birthday}</td>
											<td>${user.sex}</td>
											<td>${user.createdDate}</td>
											<td>${user.status}</td>
										</tr>
									</c:forEach>
								</tbody>
								<tfoot>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Birthday</th>
										<th>Sex</th>
										<th>Created Date</th>
										<th>Status</th>
									</tr>
								</tfoot>
							</table>
							<div class="clearfix"></div>
							<c:choose>
								<c:when test="${totalElements > pageSize}">
									<div class="center">
										<div id="page-selection" data-total-pages="${totalPages}"
											data-page-number="${pageNumber}"></div>
									</div>
								</c:when>
								<c:when test="${totalElements == 0}">
									<div class="alert alert-info" role="alert">There is no user!</div>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->