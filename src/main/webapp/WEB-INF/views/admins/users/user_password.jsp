<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- row -->
<div class="row" id="userPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title">${firstName} ${lastName}</h2>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool"
						data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a href='<spring:url value="/inotes/users" />' class="btn btn-default btn-sm" title="Cancel"><i class="fa fa-mail-reply-all"></i></a>
					</span>
					<c:if test="${success == 1}">
			            <span class="text-green">
			            	<i class="icon fa fa-check"></i> Password Changed Successfully!
						</span>
					</c:if>
				</div>
			</div>
			<!-- /.box-header -->
			<form:form action="reset-password" modelAttribute="userPasswordDTO" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label> <form:errors path="email" class="form-error" />
								<form:input path="email" type="text" placeholder="Current Email" id="email" class="form-control" readonly="true"/>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Current Password</label> <form:errors path="password" class="form-error" />
								<form:input path="password" type="password" placeholder="Current Password" id="password" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">New Password</label> <form:errors path="newPassword" class="form-error" />
								<form:input path="newPassword" type="password" placeholder="New Password" id="newPassword" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Confirm Password</label> <form:errors path="confirmPassword" class="form-error" />
								<form:input path="confirmPassword" type="password" placeholder="Confirm Password" id="confirmPassword" class="form-control" />
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer with-border">
					<input type="submit" value="Change" class="btn btn-primary">
				</div>
			</form:form>
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->