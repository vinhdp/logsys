<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- row -->
<div class="row" id="userPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title">${firstName} ${lastName}</h2>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool"
						data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a href='<spring:url value="/inotes/users" />' class="btn btn-default btn-sm" title="Cancel"><i class="fa fa-mail-reply-all"></i></a>
					</span>
					<c:if test="${success == 1}">
			            <span class="text-green">
			            	<i class="icon fa fa-check"></i> Permission Changed Successfully!
						</span>
					</c:if>
				</div>
			</div>
			<!-- /.box-header -->
			<form action="edit-permission" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label> <form:errors path="email" class="form-error" />
								<input type="text" value="${email}" placeholder="Current Email" id="email" class="form-control" readonly="true"/>
							</div>
							<div class="form-group">
								<label>
									<div class="icheckbox">
										<input type="checkbox" class="minimal" id="per-admin" /> ADMIN
									</div>
								</label>
								
								<label>
									<div class="icheckbox">
										<input type="checkbox" class="minimal" id="per-mod" /> MODERATOR
									</div>
								</label>
								
								<label>
									<div class="icheckbox">
										<input type="checkbox" class="minimal" id="per-user" /> USER
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer with-border">
					<input type="submit" value="Change" class="btn btn-primary">
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->