<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- row -->
<div class="row" id="userPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title">${userDTO.firstName} ${userDTO.lastName}</h2>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool"
						data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a href='<spring:url value="/inotes/user/${userDTO.id}/update" />' class="btn btn-default btn-sm" title="Update user"><i class="fa fa-pencil-square-o"></i></a>
						<a id="delete" class="btn btn-default btn-sm"  title="Delete this user"><i class="fa fa-trash-o"></i></a>
						<a href='<spring:url value="/inotes/user/${userDTO.email}/reset-password" />' class="btn btn-default btn-sm" title="Reset Password"><i class="fa fa-lock"></i></a>
						<a href='<spring:url value="/inotes/user/${userDTO.email}/edit-permission" />' class="btn btn-default btn-sm" title="Edit Permission"><i class="fa fa-shield "></i></a>
						<c:if test="${success == 1}">
				            <span class="text-green">
				            	<i class="icon fa fa-check"></i> User has been successfully added!
							</span>
						</c:if>
					</span>
					<span id="confirmDeletion" class="hidden">
			            <span class="text-red"><i class="icon fa fa-exclamation-triangle"></i> Deletion Warning! Do you want to delete this category?
						</span>
						<a href='<spring:url value="/inotes/user/${userDTO.id}/delete" />' class="" title="Delete immediately!"><i class="fa fa-check"></i></a>
						
						<a href='#' id="discardDeletion" title="Discard action!"><i class="fa fa-times"></i></a>
					</span>
					<span id="controlGroup" class="pull-right">
						<a href='<spring:url value="/inotes/seo/update?url=/member/${userDTO.firstName}" />' class="btn btn-default btn-sm" title="Edit SEO Information"><i class="fa fa-globe"></i></a>
					</span>
				</div>
			</div>
			<!-- /.box-header -->
			<form:form action="${userDTO.id}/update" modelAttribute="userDTO" method="post">
				<div class="box-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="exampleInputEmail1">First Name</label>
								<form:input path="firstName" type="text" placeholder="First Name" id="firstName" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Last Name</label>
								<form:input path="lastName" type="text" placeholder="Last Name" id="lastName" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Birthday</label>
								<form:input path="birthday" type="text" placeholder="Birthday" id="birthday" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Created Date</label>
								<form:input path="createdDate" type="text" placeholder="Created Date" id="createdDate" class="form-control" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label>
								<form:input path="email" type="text" placeholder="Email" id="email" class="form-control" />
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Sex</label>
								<form:select path="sex" data-sex-value="${userDTO.sex}"
									class="form-control select2 select2-hidden-accessible"
									style="width: 100%;" tabindex="-1" aria-hidden="true">
									<form:option value="MALE" label="MALE"></form:option>
									<form:option value="FEMALE" label="FEMALE"></form:option>
									<form:option value="UNKNOW" label="UNKNOW"></form:option>
								</form:select>
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Status</label>
								<form:select path="status" data-status-value="${userDTO.status}"
									class="form-control select2 select2-hidden-accessible"
									style="width: 100%;" tabindex="-1" aria-hidden="true">
									<form:option value="NOT_VERIFIED" label="NOT_VERIFIED"></form:option>
									<form:option value="ACTIVE" label="ACTIVE"></form:option>
									<form:option value="INACTIVE" label="INACTIVE"></form:option>
									<form:option value="BANNED" label="BANNED"></form:option>
									<form:option value="BLOCKED" label="BLOCKED"></form:option>
								</form:select>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer with-border">
					<a href="${userDTO.id}/update" class="btn btn-primary">Update</a>
				</div>
			</form:form>
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->