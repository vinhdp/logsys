<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- row -->
<div class="row" id="userPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h2 class="box-title">${seo.url}</h2>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool"
						data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<a id="showall" href='<spring:url value="/inotes/seos" />' class="btn btn-default btn-sm"  title="Show alls"><i class="fa fa-list-alt"></i></a>
						<a id="delete" class="btn btn-default btn-sm"  title="Delete this info"><i class="fa fa-trash-o"></i></a>
					</span>
					<span id="confirmDeletion" class="hidden">
			            <span class="text-red"><i class="icon fa fa-exclamation-triangle"></i> Deletion Warning! Do you want to delete this seo info?
						</span>
						<a href='<spring:url value="/inotes/seo/delete-url?url=${seo.url}" />' class="" title="Delete immediately!"><i class="fa fa-check"></i></a>
						
						<a href='#' id="discardDeletion" title="Discard action!"><i class="fa fa-times"></i></a>
					</span>
				</div>
			</div>
			<c:if test="${flag == 1}">
				<div class="box-body">
					<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                <h4><i class="icon fa fa-check"></i> Update successfully!!</h4>
		                ${seo.url}
		              </div>
		        </div>
			</c:if>
			<!-- /.box-header -->
			<form:form action="update-url" modelAttribute="seo" method="post">
				<div class="box-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>URL</label> <form:errors path="url" class="form-error" />
							<form:textarea path="url" rows="3" placeholder="URL" id="url" class="form-control" />
						</div>
						<div class="form-group hidden">
							<label>ID</label> <form:errors path="id" class="form-error" />
							<form:textarea path="id" rows="3" placeholder="id" id="id" class="form-control" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="exampleInputPassword1">Type</label> <form:errors path="type" class="form-error" />
							<form:select path="type" data-type-value="${seo.type}"
								class="form-control select2 select2-hidden-accessible"
								style="width: 100%;" tabindex="-1" aria-hidden="true">
								<form:option value="PAGE" label="PAGE"></form:option>
								<form:option value="POST" label="POST"></form:option>
								<form:option value="CATEGORY" label="CATEGORY"></form:option>
								<form:option value="TAG" label="TAG"></form:option>
								<form:option value="USER" label="USER"></form:option>
							</form:select>
						</div>
					</div>
				</div>
					<div class="form-group">
						<label>Title</label> <form:errors path="title" class="form-error" />
						<form:textarea path="title" rows="2" placeholder="Title" id="title" class="form-control" />
					</div>
					<div class="form-group">
						<label>Keyword</label> <form:errors path="keyword" class="form-error" />
						<form:textarea path="keyword" rows="2" placeholder="Keyword" id="keyword" class="form-control" />
					</div>
					<div class="form-group">
						<label>Description</label> <form:errors path="description" class="form-error" />
						<form:textarea path="description" rows="4" placeholder="Description" id="description" class="form-control" />
					</div>
					<div class="form-group">
						<label>Image</label> <form:errors path="image" class="form-error" />
						<form:textarea path="image" rows="2" placeholder="Image" id="image" class="form-control" />
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer with-border">
					<input type="submit" value="Save" class="btn btn-primary">
				</div>
			</form:form>
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->