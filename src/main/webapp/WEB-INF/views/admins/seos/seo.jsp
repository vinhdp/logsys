<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- row -->
<div class="row" id="seoPage">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">All SEO Information</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="mailbox-controls" id="controlMenu">
					<!-- Check all button -->
					<span id="controlGroup">
						<div class="row">
							<div class="col-md-1 p-b-5">
								<a href='<spring:url value="/inotes/seo/update-url?url=insert-your-url" />' class="btn btn-default btn-sm" title="Add new seo information"><i class="fa fa-plus-square"></i></a>
							</div>
							<div class="col-md-4 p-b-5">
								<select id="category" class="form-control select2 select2-hidden-accessible"
									tabindex="-1" aria-hidden="true"  style="width: 100%">
									<option value="0">ALL</option>
									
								</select>
							</div>
							<div class="box-tools pull-right col-xs-12 col-sm-12 col-md-6">
			                	<div class="has-feedback">
			                    	<input id="searchBox" ng-model="searchTitle" ng-change="textChanged()" type="text" class="form-control" placeholder="Search Post">
			                    	<span class="glyphicon glyphicon-search form-control-feedback"></span>
			                    </div>
			           		</div>
		               </div>
					</span>
				</div>
			</div>
			<div class="box-body">
				<div id="example2_wrapper"
					class="dataTables_wrapper form-inline dt-bootstrap">
					<div class="row">
						<div class="col-sm-12">
							<table id="allSEOs" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>URL</th>
										<th>Title</th>
										<th>Keyword</th>
										<th>Description</th>
										<th>Type</th>
									</tr>
								</thead>
								<tbody id="tableBody">
									<c:forEach var="seo" items="${lstSEOs}">
										<tr>
											<c:choose>
												<c:when test="${seo.url != null}">
													<td><a href='<spring:url value="/inotes/seo/update-url?url=${seo.url}" />'>${seo.url}</a></td>
												</c:when>
												<c:otherwise>
													<td><a href='<spring:url value="/inotes/seo/update?url=${seo.id}" />'>${seo.id}</a></td>
												</c:otherwise>
											</c:choose>
											<td>${seo.title}</td>
											<td>${seo.keyword}</td>
											<td>${seo.description}</td>
											<td>${seo.type}</td>
										</tr>
									</c:forEach>
									<tr>
										<th>URL</th>
										<th>Title</th>
										<th>Keyword</th>
										<th>Description</th>
										<th>Type</th>
									</tr>
								</tbody>
							</table>
							<div class="clearfix"></div>
							<c:choose>
								<c:when test="${totalElements > pageSize}">
									<div class="center">
										<div id="page-selection" data-total-pages="${totalPages}"
											data-page-number="${pageNumber}"></div>
									</div>
								</c:when>
								<c:when test="${totalElements == 0}">
									<div class="alert alert-info" role="alert">There is no seo info!</div>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- #row -->