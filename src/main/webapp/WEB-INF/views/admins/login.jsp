<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href='<spring:url value="/resources/components/bootstrap/css/bootstrap.min.css"></spring:url>'>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href='<spring:url value="/resources/components/adminlte2/css/AdminLTE.min.css"></spring:url>'>
    <!-- iCheck -->
    <link rel="stylesheet" href='<spring:url value="/resources/components/adminlte2/plugins/iCheck/square/blue.css"></spring:url>'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href='<spring:url value="/"></spring:url>'><b>Tech Notes</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
      	<c:if test="${not empty message}">
      		<p class="login-box-msg">${message}</p>
      	</c:if>
      	<c:if test="${not empty error}">
      		<p class="login-box-msg has-error">
      			<label class="control-label" for="inputError">
      				<i class="fa fa-times-circle-o"></i> ${error}
                </label>
      		</p>
      	</c:if>
        <c:if test="${not empty msg}">
      		<p class="login-box-msg has-success">
      			<label class="control-label" for="inputError">
      				<i class="fa fa-check"></i> ${msg}
                </label>
      		</p>
      	</c:if>
        <form:form action="login" method="post" modelAttribute="user">
          <div class="form-group has-feedback">
          	<form:input path="email" type="email" class="form-control" placeholder="Email" value=""/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <form:input path="password" type="password" class="form-control" placeholder="Password" value=""/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form:form>

        <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div><!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
        <a href="#" class="text-center">Register a new membership</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src='<spring:url value="/resources/components/jquery/jquery-2.2.0.min.js"></spring:url>'></script>
    <!-- Bootstrap 3.3.5 -->
    <script src='<spring:url value="/resources/components/bootstrap/js/bootstrap.min.js"></spring:url>'></script>
    <!-- iCheck -->
    <script src='<spring:url value="/resources/components/adminlte2/plugins/iCheck/icheck.min.js"></spring:url>'></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>