<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="col-md-8 col-sm-7">
	<div class="panel panel-primary">
		<ul class="list-group">
			<li class="list-group-item">
				<h1>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</h1>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
				<p>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</p>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
				<p>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</p>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
				<p>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</p>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
				<p>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</p>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
				<p>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</p>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
				<p>
					<strong>Demo 3:</strong> Overlapping levels with back links
				</p>
				<p>This menu will open by pushing the website content to the
					right. It has multi-level functionality, allowing endless nesting
					of navigation elements.</p>
				<p>The next level can optionally overlap or cover the previous
					one.</p>
			</li>
		</ul>
	</div>
</div>