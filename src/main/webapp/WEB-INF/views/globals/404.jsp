<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="col-md-8 col-sm-7">
	<section class="content">
		<div class="error-page">
			<div class="error-content">
				<h3>
					<i class="fa fa-warning text-yellow"></i> Oops! Page not found.
				</h3>
				<p>
					We could not find the page you were looking for. Meanwhile, you may
					<a href='<spring:url value="/"></spring:url>'>return to Home Page</a> or try using the
					search form.
				</p>
				<form action='<spring:url value="/search"></spring:url>'>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<input type="text" name="q" class="form-control input-search"
									placeholder="Search..." value="${q}">
									<div class="input-group-btn">
									<button type="submit"
										class="btn btn-primary btn-flat">
										<i class="fa fa-search"></i>
									</button>
								</div>
							</div>
							<!-- /input-group -->
						</div>
					</div>
					<!-- /.row -->
				</form>
			</div>
			<!-- /.error-content -->
		</div>
		<!-- /.error-page -->
	</section>
	<!-- /.content -->
</div>