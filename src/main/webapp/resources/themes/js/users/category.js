// init bootpag
$('#page-selection').bootpag({
	total: $("#page-selection").data("totalPages"),
	page: $("#page-selection").data("pageNumber") + 1,
	href: "?page={{number}}",
	maxVisible: 5,
	leaps : false,
    firstLastUse: true,
    first: '←',
    last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    nextClass: 'next',
    prevClass: 'prev',
    lastClass: 'last',
    firstClass: 'first'
}).on("page", function(event, num) {
	
});
if($("#appPost").length > 0) {
	var posts = angular.module('posts', ['ui.bootstrap']).filter('timeAgo', ['$interval', function ($interval){
	    // trigger digest every 60 seconds
	    $interval(function (){}, 60000);

	    function fromNowFilter(time){
	      return moment(time).fromNow();
	    }

	    fromNowFilter.$stateful = true;
	    return fromNowFilter;
	  }]);;
	
	posts.controller('PostController', function($scope, $http, $location, $anchorScroll) {
		
		$scope.currentPage = 1;
		$scope.numPerPage = 9;
		var url = baseUrl("/rest/category/" + $("#categories").data("id") + "/post");
		$scope.$watch('currentPage', function() {
			
			$http.get(url,{ params: {page: $scope.currentPage}})
			.then(function(res){
				
				$scope.lstPosts = res.data.content;
				$scope.totalItems = res.data.totalElements;
		    	$scope.currentPage = res.data.number + 1;
		    	$scope.numPerPage = res.data.size;
		    	$scope.maxSize = 5;
		    	if($scope.totalItems <= $scope.numPerPage){
		    		
		    		$scope.visible = true;
		    	}
			});
	
			$location.hash('top');
			$anchorScroll();
			$location.hash('');
		});
	});
	
	/* Resource: http://plnkr.co/edit/81fPZxpnOQnIHQgp957q?p=preview */
	var categories = angular.module('categories', ['ui.bootstrap']);
	
	categories.controller('CategoryController', function($scope, $http, $location, $anchorScroll) {
		
		$scope.currentPage = 1;
		$scope.numPerPage = 9;
		var url = baseUrl("/rest/category/" + $("#categories").data("id"));
		$scope.$watch('currentPage + numPerPage', function() {
	
			$http.get(url,{ params: {page: $scope.currentPage}})
			.then(function(res){
				
				$scope.lstCategories = res.data.content;
				$scope.totalItems = res.data.totalElements;
		    	$scope.currentPage = res.data.number + 1;
		    	$scope.numPerPage = res.data.size;
		    	$scope.maxSize = 5;
		    	if($scope.totalItems <= $scope.numPerPage){
		    		
		    		$scope.visible = true;
		    	}
			});
			
			$location.hash('top');
			$anchorScroll();
			$location.hash('');
		});
	});
	
	angular.bootstrap(document.getElementById("appCategory"),['categories']);
}