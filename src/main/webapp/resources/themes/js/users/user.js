// init bootpag
$('#page-selection').bootpag({
	total: $("#page-selection").data("totalPages"),
	page: $("#page-selection").data("pageNumber") + 1,
	href: "?page={{number}}",
	maxVisible: 5,
	leaps : false,
    firstLastUse: true,
    first: '←',
    last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    nextClass: 'next',
    prevClass: 'prev',
    lastClass: 'last',
    firstClass: 'first'
});