// Integrate with all existing and future CKEditor instances.

var url = baseUrl('/resources/components/ckfinder/');

CKFinder.setupCKEditor( null, { basePath : url, height: '100%' } ) ;
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
CKEDITOR.replace('editor', {
	/*extraAllowedContent: 'select[*](*)'*/
});