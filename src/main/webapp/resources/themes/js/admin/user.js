$(function() {
	$('#allUsers').DataTable({
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : true,
		"info" : false,
		"autoWidth" : false
	});
	
	if($("#status").length > 0) {
		
		var $select2Status = $("#status").select2({
			minimumResultsForSearch: Infinity
		});
		var statusValue = $("#status").data("statusValue");
		$select2Status.val(statusValue).trigger("change");
	}
	
	if($("#sex").length > 0) {
		
		var $select2Sex = $("#sex").select2({
			minimumResultsForSearch: Infinity
		});
		var sexValue = $("#sex").data("sexValue");
		
		$select2Sex.val(sexValue).trigger("change");
	}
	
	if($("#per-admin").length > 0) {
	
		$("#per-admin").iCheck({
		    checkboxClass: 'icheckbox_minimal-blue',
		    radioClass: 'iradio_minimal-blue',
		    increaseArea: '20%' // optional
		});
		
		$("#per-mod").iCheck({
		    checkboxClass: 'icheckbox_minimal-blue',
		    radioClass: 'iradio_minimal-blue',
		    increaseArea: '20%' // optional
		});
		
		$("#per-user").iCheck({
		    checkboxClass: 'icheckbox_minimal-blue',
		    radioClass: 'iradio_minimal-blue',
		    increaseArea: '20%' // optional
		});
	}
});

$('#page-selection').bootpag({
	total: $("#page-selection").data("totalPages"),
	page: $("#page-selection").data("pageNumber") + 1,
	href: "?page={{number}}",
	maxVisible: 5,
	leaps : false,
    firstLastUse: true,
    first: '←',
    last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    nextClass: 'next',
    prevClass: 'prev',
    lastClass: 'last',
    firstClass: 'first'
}).on("page", function(event, num) {
	
});

$(document).ready(function(){
	
	$("#delete").on("click", function(){
		$("#confirmDeletion").removeClass("hidden");
		$("#controlGroup").addClass("hidden");
	});
	
	$("#discardDeletion").on("click", function(e){
		$("#confirmDeletion").addClass("hidden");
		$("#controlGroup").removeClass("hidden");
		e.preventDefault();
	});
});