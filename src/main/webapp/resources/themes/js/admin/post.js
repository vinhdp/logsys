$(function() {
	var postTable = $('#allPosts').DataTable({
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false,
		"info" : false,
		"autoWidth" : false
	});
	
	var $select2Category = $("#category").select2();
	var $select2Status = $("#status").select2({
		minimumResultsForSearch: Infinity
	});
	
	var category = $("#category").data("categoryId");
	var statusValue = $("#status").data("statusValue");
	
	$select2Category.val(category).trigger("change");
	$select2Status.val(statusValue).trigger("change");
	
});

$('#page-selection').bootpag({
	total: $("#page-selection").data("totalPages"),
	page: $("#page-selection").data("pageNumber") + 1,
	href: "?page={{number}}",
	maxVisible: 5,
	leaps : false,
    firstLastUse: true,
    first: '←',
    last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    nextClass: 'next',
    prevClass: 'prev',
    lastClass: 'last',
    firstClass: 'first'
}).on("page", function(event, num) {
	
});

$(document).ready(function(){
	
	$("#delete").on("click", function(){
		$("#confirmDeletion").removeClass("hidden");
		$("#controlGroup").addClass("hidden");
	});
	
	$("#discardDeletion").on("click", function(e){
		$("#confirmDeletion").addClass("hidden");
		$("#controlGroup").removeClass("hidden");
		e.preventDefault();
	});
	
	$("#title").on("input", function() {
		var title = $(this).val();
		var alias = stringNormalizer(title);
		$("#alias").val(alias);
	});
});

var postsApp = angular.module('posts', ['ui.bootstrap']);
//var categoryId = $("#category").val();

postsApp.controller('PostController', function($scope, $http, $location, $anchorScroll, $timeout) {
	
	$scope.currentPage = 1;
	$scope.numPerPage = 20;
	$scope.searchTitle = "";
	$scope.category = 0;
	
	$scope.textChanged = function() {
		
		//reset current page
		//categoryId = $("#category").val();
		$scope.currentPage = 1;
		$scope.numPerPage = 20;
		
		$("#tableBody").children().not(':last').remove();
    };
    
    $scope.$watch('currentPage + searchTitle + category', function() {
		
    	var title = $scope.searchTitle;
    	var url = baseUrl("/inotes/post/rest/category/" + $scope.category + "?title=" + title);
		
    	$http.get(url,{ params: {page: $scope.currentPage}})
		.then(function(res){
			
			$scope.lstPosts = res.data.content;
			$scope.totalItems = res.data.totalElements;
	    	//console.log("Total Items: " + $scope.totalItems)
	    	$scope.currentPage = res.data.number + 1;
	    	//console.log("Current Page: " + $scope.currentPage)
	    	$scope.numPerPage = res.data.size;
	    	//console.log("Number Per page: " + $scope.numPerPage)
	    	$scope.maxSize = 5;
	    	//console.log("Call events: curPage: " + $scope.currentPage);
	    	//console.log("Max Size: " + $scope.maxSize);
	    	if($scope.totalItems <= $scope.numPerPage){
	    		
	    		$scope.visible = true;
	    		//console.log("Visible Size: true");
	    	}else{
	    		
	    		$scope.visible = false;
	    	}
			/*angular.forEach(res.data.content, function(value, key){
		         postTable.row.add([value.])
		           console.log("username is thomas");
		    });*/
		});

		$location.hash('top');
		$anchorScroll();
		$location.hash('');
	});
    
    $scope.formatDateTime = function()
    {
         $timeout(function() {
        	$('.datetime').each(function(){
        		
        		var timestamp = $(this).text();
        		if(timestamp.length > 0){
	        		
        			var dateStr = moment.unix(timestamp / 1000).format("YYYY-MM-DD HH:mm:ss");
	        		$(this).text(dateStr);
        		}
        	}); 
         }, 0); // wait...
    };
});

postsApp.directive('afterRender', function() {
	return function(scope, element, attrs) {
		if (scope.$last) { // all are rendered
			scope.$eval(attrs.afterRender);
		}
	}
});
