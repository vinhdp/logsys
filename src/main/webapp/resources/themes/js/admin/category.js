$(function() {
	$('#allCategories').DataTable({
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : true,
		"info" : true,
		"autoWidth" : false
	});
	
	if($("#superCategory").length > 0){
		
		var $select2Category = $("#superCategory").select2();
		var superCategory = $("#superCategory").data("superId");
		$select2Category.val(superCategory).trigger("change");
	}
	
	if($("#status").length > 0){
		
		var $select2Status = $("#status").select2({
			minimumResultsForSearch: Infinity
		});
		var statusValue = $("#status").data("statusValue");
		$select2Status.val(statusValue).trigger("change");
	}
	$("#title").on("input", function() {
		var title = $(this).val();
		var alias = stringNormalizer(title);
		$("#alias").val(alias);
	});
});

$(document).ready(function(){
	
	$("#delete").on("click", function(){
		$("#confirmDeletion").removeClass("hidden");
		$("#controlGroup").addClass("hidden");
	});
	
	$("#discardDeletion").on("click", function(e){
		$("#confirmDeletion").addClass("hidden");
		$("#controlGroup").removeClass("hidden");
		e.preventDefault();
	});
});