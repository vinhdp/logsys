$(function() {
	
	var seoTable = $('#allSEOs').DataTable({
		"paging" : false,
		"lengthChange" : false,
		"searching" : false,
		"ordering" : false,
		"info" : false,
		"autoWidth" : false
	});
	
	var $select2Type = $("#type").select2({
		minimumResultsForSearch: Infinity
	});
	var typeValue = $("#type").data("typeValue");
	if (typeValue === undefined){
		typeValue = "PAGE";
	}
	$select2Type.val(typeValue).trigger("change");
});

$(document).ready(function(){
	
	$("#delete").on("click", function(){
		$("#confirmDeletion").removeClass("hidden");
		$("#controlGroup").addClass("hidden");
	});
	
	$("#discardDeletion").on("click", function(e){
		$("#confirmDeletion").addClass("hidden");
		$("#controlGroup").removeClass("hidden");
		e.preventDefault();
	});
});

$('#page-selection').bootpag({
	total: $("#page-selection").data("totalPages"),
	page: $("#page-selection").data("pageNumber") + 1,
	href: "?page={{number}}",
	maxVisible: 5,
	leaps : false,
    firstLastUse: true,
    first: '←',
    last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    nextClass: 'next',
    prevClass: 'prev',
    lastClass: 'last',
    firstClass: 'first'
}).on("page", function(event, num) {
	
});