/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
var context = "";
//context = "/logsys"

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	/*config.font_defaultLabel = 'Arial';
	config.fontSize_defaultLabel = '44px';*/
	config.contentsCss
	config.height = 500;
	config.filebrowserImageBrowseUrl = context + '/ckfinder/ckfinder.html?type=Images';
	config.filebrowserImageUploadUrl = context + '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images';
};
