﻿/*
Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
For licensing, see license.txt or http://cksource.com/ckfinder/license
*/
var context = "";
//context = "/logsys";

CKFinder.customConfig = function( config )
{
	config.filebrowserBrowseUrl = context + '/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = context + '/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = context + '/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = context + '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = context + '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = context + '/ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash';
	
	config.filebrowserWindowWidth = '1000';
	config.filebrowserWindowHeight = '700';
	config.uiColor = '#fff';   
	config.toolbar = 'Full';
	config.font_names = '/;/;/_GB2312;/_GB2312;/;/;/;';

};
